Khallware Ramsey-on-You
=================
Overview
---------------
This android application empowers users to predict their financial future
following the principles espoused by Dave Ramsey.

```shell
chromium-browser https://play.google.com/store/apps/details?id=com.khallware.roy&hl=en
```

First-time Only
---------------

```shell
chromium-browser https://developer.android.com/studio/index.html
# download android studio ide
sudo unzip -d /usr/local android-studio-ide-*-linux.zip
rm android-studio-ide-*-linux.zip
export PATH=$PATH:/usr/local/android-studio/bin/
studio.sh
export PATH=$PATH:$HOME/Android/Sdk/tools/
android
# Android SDK build-tools v22.0.1
# Android 5.1.1 - SDK Platform v2
# Android 5.1.1 - Google APIs v1
# Android 5.1.1 - Intel x86 Atom_64 System Image
# accept license and install
android update sdk --no-ui --obsolete --force
```

If you are running under centos7 minimal and have the required dependencies
already, ignore the following:

```shell
docker run -it -h build-roy --name build-roy -v $HOME/Android:/usr/local/Android centos
yum install -y git maven-3.0.5 java-1.8.0-openjdk-devel
mkdir -p ~/.m2/
cat <<'EOF' >~/.m2/settings.xml
<settings xmlns="http://maven.apache.org/POM/4.0.0"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
      http://maven.apache.org/xsd/settings-1.0.0.xsd">
   <profiles>
      <profile>
         <id>roy-properties</id>
         <properties>
            <android.sdk.path>/usr/local/Android/Sdk</android.sdk.path>
         </properties>
      </profile>
   </profiles>
   <activeProfiles>
      <activeProfile>roy-properties</activeProfile>
   </activeProfiles>
</settings>
EOF
```
Build
---------------

```shell
git clone https://gitlab.com/harkwell/roy.git && cd roy
export ANDROID_HOME=/home/khall/Android/Sdk/
cd roy-lib && mvn package
mvn install:install-file -Dfile=target/roy.jar -DgroupId=com.khallware.roy -DartifactId=khallware-roy -Dversion=0.2 -Dpackaging=jar
cd .. && mvn package
ls -ld target/RoY.apk
```

Deploy
---------------

```shell
# first time
android create avd -n fovea -t 26
android list targets
mksdcard 256M ~/tmp/sdcard1.iso
emulator -sdcard ~/tmp/sdcard1.iso -avd fovea
# on phones, don't forget to set "settings" -> "security" -> "unknown sources"

# re-install
adb uninstall com.khallware.roy
adb install target/RoY.apk
```


Releases
---------------

```shell
# tag release (eg "roy_1_0_0")
# checkout source from tag

git checkout roy_1_0_0
SDKVER=$(android list targets |grep ^id: |tail -1 |awk '{print $2}')
mvn package -Dbuild.apkver=$(date +%Y%m%d) -Dbuild.sdkver=$SDKVER -Dbuild.incr=$(date +%Y%m%d)-$((count++))

# release apk
cp keystore.jks /tmp/
mvn package -Dbuild.apkver=$(date +%Y%m%d) -Dbuild.sdkver=$SDKVER -Dbuild.incr=$(date +%Y%m%d)-$((count++)) -Dandroid.release=true -Dkeystore.password=MYPASSWORD -Dsigning-keystore-alias=hall.dnsalias.net -Psign
```
