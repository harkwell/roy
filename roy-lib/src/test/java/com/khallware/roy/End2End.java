/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.io.FileInputStream;
import java.util.Properties;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class End2End
{
	public static final String PROPFILE = "test.properties";

	private Logger logger = LoggerFactory.getLogger(End2End.class);

	@Test
	public void calcTest() throws Exception
	{
		Properties props = new Properties();
		props.load(new FileInputStream(PROPFILE));
		try {
			new RamseyOnYou(props).exec();
		}
		catch (RetirementException e) {
			logger.info(""+e, e);
		}
	}
}
