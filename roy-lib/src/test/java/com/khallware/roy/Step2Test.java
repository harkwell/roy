/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Properties;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import org.junit.Test;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.assertTrue;

public class Step2Test

{
	private Logger logger = LoggerFactory.getLogger(Step2Test.class);
	private Step2 step2 = null;
	private List<Record> timeline = null;
	private DateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);

	@Before
	public void Step2Test() throws Exception
	{
		step2 = new Step2();
		timeline = new ArrayList<>();
		timeline.add(new MockRecord());
		step2.setTimeline(timeline);
	}

	@Test
	public void negativeSavingsTest() throws Exception
	{
		step2.getRecord().setFigure(Figure.monthlySavings,
			new Double(-1000));
		try {
			step2.calculate();
			logger.info(step2.summarize());
			assertTrue(false);
		}
		catch (FatalException e) {
			logger.info(""+e);
		}
		assertTrue(true);
	}

	@Test
	public void slowSavingsTest() throws Exception
	{
		step2.getRecord().setFigure(Figure.monthlySavings,
			new Double(0.01));
		try {
			step2.calculate();
			logger.info(step2.summarize());
			assertTrue(false);
		}
		catch (RetirementException e) {
			logger.info(""+e);
		}
		assertTrue(true);
	}

	@Test
	public void saneTest() throws Exception
	{
		Date date = null;
		step2.getRecord().setFigure(Figure.monthlySavings,
			new Double(2000.00));
		step2.calculate();
		date = step2.getRecord().getDate(Event.currentDate);
		logger.info(step2.summarize());
		assertTrue(df.format(date).equals("20140426"));
	}

	@Test
	public void acceleratedTest() throws Exception
	{
		Date date = null;
		step2.getRecord().setFigure(Figure.currRetireSavings,
			new Double(2000.00));
		step2.calculate();
		date = step2.getRecord().getDate(Event.currentDate);
		logger.info(step2.summarize());
		assertTrue(df.format(date).equals("20150819"));
	}
}
