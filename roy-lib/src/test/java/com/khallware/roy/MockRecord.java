/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class MockRecord extends Record

{
	private DateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
	private Logger logger = LoggerFactory.getLogger(MockRecord.class);

	public MockRecord() throws ParseException, FatalException
	{
		setFigure(Figure.salary, 86700.00);
		setFigure(Figure.monthlySavings, 1000.00);
		setFigure(Figure.emergencyFund, 0.00);
		setFigure(Figure.capital, 500.00);
		setFigure(Figure.currRetireSavings, 0.00);
		setFigure(Figure.collegeBalance, 0.00);
		setFigure(Figure.mortgageDebt, 273769.64);
		setFigure(Figure.nonMortgageDebt, 31503.14);
		setFigure(Figure.moRetireSavings, 401.23);
		setFigure(Figure.currMoCollegeSavings, 0.00);
		setFigure(Figure.numEmergencyMonths, 6.0);
		setFigure(Figure.mortgageInt, 0.0425);
		setFigure(Figure.houseValue, 350000.00);
		setFigure(Figure.mortgageLoan, 280000.00);
		setFigure(Figure.mortgageLength, 30.0);
		setFigure(Figure.mortgagePayment, 1438.99);
		setFigure(Figure.savingsInt, 0.0005);
		setFigure(Figure.marketRate, 0.15);
		setFigure(Figure.nestEgg, 0.00);
		setDate(Event.birth, df.parse("19700101"));
		setDate(Event.death, df.parse("20700101"));
		setDate(Event.retirement, df.parse("20400101"));
		setDate(Event.currentDate, df.parse("20130101"));
		validate();
	}
}
