/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Properties;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import org.junit.Test;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.assertTrue;

public class Step5Test

{
	private Logger logger = LoggerFactory.getLogger(Step5Test.class);
	private Step5 step5 = null;
	private List<Record> timeline = null;
	private DateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);

	@Before
	public void Step5Test() throws Exception
	{
		step5 = new Step5();
		timeline = new ArrayList<>();
		timeline.add(new MockRecord());
		step5.setTimeline(timeline);
	}

	@Test
	public void negativeSavingsNoKidsTest() throws Exception
	{
		step5.getRecord().setFigure(Figure.monthlySavings,
			new Double(-1000));
		try {
			step5.calculate();
			logger.info(step5.summarize());
			assertTrue(false);
		}
		catch (FatalException e) {
			logger.info(""+e);
		}
		assertTrue(true);
	}

	@Test
	public void negativeSavingsTest() throws Exception
	{
		step5.getRecord().setFigure(Figure.monthlySavings,
			new Double(-1000));
		step5.getRecord().setKid(df.parse("20070101"));
		step5.getRecord().setKid(df.parse("20090202"));
		step5.getRecord().setKid(df.parse("20110303"));
		try {
			step5.calculate();
			logger.info(step5.summarize());
			assertTrue(false);
		}
		catch (FatalException e) {
			logger.info(""+e);
		}
		assertTrue(true);
	}

	@Test
	public void slowSavingsTest() throws Exception
	{
		step5.getRecord().setFigure(Figure.monthlySavings,
			new Double(0.01));
		step5.getRecord().setKid(df.parse("20070101"));
		step5.getRecord().setKid(df.parse("20090202"));
		step5.getRecord().setKid(df.parse("20110303"));
		try {
			step5.calculate();
			logger.info(step5.summarize());
			assertTrue(false);
		}
		catch (RetirementException e) {
			logger.info(""+e);
		}
		assertTrue(true);
	}

	@Test
	public void saneTest() throws Exception
	{
		Date date = null;
		step5.getRecord().setFigure(Figure.monthlySavings,
			new Double(2000.00));
		step5.getRecord().setKid(df.parse("20070101"));
		step5.getRecord().setKid(df.parse("20090202"));
		step5.getRecord().setKid(df.parse("20110303"));
		step5.calculate();
		date = step5.getRecord().getDate(Event.currentDate);
		logger.info(step5.summarize());
		assertTrue(df.format(date).equals("20130630"));
	}
}
