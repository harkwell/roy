/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Properties;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import org.junit.Test;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.assertTrue;

public class Step7Test
{
	private Logger logger = LoggerFactory.getLogger(Step7Test.class);
	private Step7 step7 = null;
	private List<Record> timeline = null;
	private DateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);

	@Before
	public void Step7Test() throws Exception
	{
		Record record = new MockRecord();
		timeline = new ArrayList<>();
		step7 = new Step7();
		record.setFigure(Figure.mortgageDebt, 0.00);
		record.setFigure(Figure.nonMortgageDebt, 0.00);
		timeline.add(record);
		step7.setTimeline(timeline);
	}

	@Test
	public void negativeSavingsTest() throws Exception
	{
		logger.info("negativeSavingsTest()...");
		step7.getRecord().setFigure(Figure.monthlySavings,
			new Double(-1000));
		try {
			step7.calculate();
			logger.info(step7.summarize());
			assertTrue(false);
		}
		catch (FatalException e) {
			logger.info(""+e);
		}
		assertTrue(true);
	}

	@Test
	public void slowSavingsTest() throws Exception
	{
		logger.info("slowSavingsTest()...");
		step7.getRecord().setFigure(Figure.monthlySavings,
			new Double(0.01));
		try {
			step7.calculate();
			logger.info(step7.summarize());
			assertTrue(false);
		}
		catch (RetirementException e) {
			logger.info(""+e);
		}
		assertTrue(true);
	}

	@Test
	public void saneTest() throws Exception
	{
		Date date = null;
		logger.info("saneTest()...");
		step7.getRecord().setFigure(Figure.monthlySavings,
			new Double(4000.00));
		step7.calculate();
		logger.info(step7.summarize());
		date = step7.getRecord().getDate(Event.currentDate);
		assertTrue(df.format(date).equals("20221011"));
	}
}
