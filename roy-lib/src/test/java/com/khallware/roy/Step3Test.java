/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Properties;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import org.junit.Test;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.assertTrue;

public class Step3Test

{
	private Logger logger = LoggerFactory.getLogger(Step3Test.class);
	private Step3 step3 = null;
	private List<Record> timeline = null;
	private DateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);

	@Before
	public void Step3Test() throws Exception
	{
		step3 = new Step3();
		timeline = new ArrayList<>();
		timeline.add(new MockRecord());
		step3.setTimeline(timeline);
	}

	@Test
	public void negativeSavingsTest() throws Exception
	{
		step3.getRecord().setFigure(Figure.monthlySavings,
			new Double(-1000));
		try {
			step3.calculate();
			logger.info(step3.summarize());
			assertTrue(false);
		}
		catch (FatalException e) {
			logger.info(""+e);
		}
		assertTrue(true);
	}

	@Test
	public void slowSavingsTest() throws Exception
	{
		Figure[] figs = new Figure[] { Figure.capital,
			Figure.moRetireSavings };

		for (Figure figure : figs) {
			step3.getRecord().setFigure(figure, 0.00);
		}
		step3.getRecord().setFigure(Figure.monthlySavings,
			new Double(0.01));
		try {
			step3.calculate();
			logger.info(step3.summarize());
			assertTrue(false);
		}
		catch (RetirementException e) {
			logger.info(""+e);
		}
		assertTrue(true);
	}

	@Test
	public void saneTest() throws Exception
	{
		Date date = null;
		step3.getRecord().setFigure(Figure.monthlySavings,
			new Double(2000.00));
		step3.calculate();
		date = step3.getRecord().getDate(Event.currentDate);
		logger.info(step3.summarize());
		assertTrue(df.format(date).equals("20140327"));
	}
}
