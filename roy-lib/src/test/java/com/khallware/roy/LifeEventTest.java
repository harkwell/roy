/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import com.khallware.roy.EventHandler.Type;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Properties;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Test;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.assertTrue;

public class LifeEventTest

{
	private Logger logger = LoggerFactory.getLogger(LifeEventTest.class);
	private Step3 step3 = null;
	private List<Record> timeline = null;
	private DateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);

	@Before
	public void LifeEventTest() throws Exception
	{
		List<EventHandler> handlers = null;
		Properties props = new Properties();
		String key = Constants.PROPHEAD+"event.20140101."+Type.raise;
		props.setProperty(key, ""+100000.00);
		timeline = new ArrayList<>();
		timeline.add(new MockRecord());
		step3 = new Step3();
		step3.setTimeline(timeline);
		handlers = Util.parseEventHandlers(props);

		for (EventHandler handler : handlers) {
			step3.register(handler);
		}
	}

	@Test
	public void simpleTest() throws Exception
	{
		Date date = null;
		try {
			logger.debug(""+step3.getRecord());
			step3.increaseSavings(500000.00);
			assertTrue(step3.getRecord().getFigure(Figure.salary)
				>= 100000.00);
		}
		catch (LifeException e) {
			logger.debug(""+e.getTranspiredDate());
			assertTrue(true);
		}
	}
}
