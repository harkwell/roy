/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Properties;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import org.junit.Test;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.assertTrue;

public class Step1Test

{
	private Logger logger = LoggerFactory.getLogger(Step1Test.class);
	private Step1 step1 = null;
	private List<Record> timeline = null;
	private DateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);

	@Before
	public void Step1Test() throws Exception
	{
		timeline = new ArrayList<>();
		step1 = new Step1();
		timeline.add(new MockRecord());
		step1.setTimeline(timeline);
	}

	@Test
	public void negativeSavingsTest() throws Exception
	{
		logger.info("negativeSavingsTest()...");
		step1.getRecord().setFigure(Figure.monthlySavings,
			new Double(-1000));
		try {
			step1.calculate();
			logger.info(step1.summarize());
			assertTrue(false);
		}
		catch (FatalException e) {
			logger.info(""+e);
		}
		assertTrue(true);
	}

	@Test
	public void slowSavingsTest() throws Exception
	{
		logger.info("slowSavingsTest()...");
		step1.getRecord().setFigure(Figure.monthlySavings,
			new Double(0.01));
		step1.getRecord().setFigure(Figure.capital, 0.00);
		try {
			step1.calculate();
			logger.info(step1.summarize());
			assertTrue(false);
		}
		catch (LifeException e) {
			logger.info(""+e);
		}
		assertTrue(true);
	}

	@Test
	public void saneTest() throws Exception
	{
		Date date = null;
		logger.info("saneTest()...");
		step1.getRecord().setFigure(Figure.monthlySavings,
			new Double(2000.00));
		step1.calculate();
		date = step1.getRecord().getDate(Event.currentDate);
		logger.info(step1.summarize());
		assertTrue(df.format(date).equals("20130131"));
	}

	@Test
	public void acceleratedTest() throws Exception
	{
		Date date = null;
		logger.info("acceleratedTest()...");
		step1.getRecord().setFigure(Figure.currRetireSavings,
			new Double(2000.00));
		step1.calculate();
		date = step1.getRecord().getDate(Event.currentDate);
		logger.info(step1.summarize());
		assertTrue(df.format(date).equals("20130131"));
	}
}
