/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.util.Date;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class Step2 extends Step
{
	private Logger logger = LoggerFactory.getLogger(Step2.class);

	private boolean redirectFunds = false;

	public Step2()
	{
		super("payoff non-mortgage-debt");
	}

	public boolean satisfied()
	{
		return(getRecord().getFigure(Figure.nonMortgageDebt) <= 0.01);
	}

	public void calculate() throws LifeException, FatalException
	{
		double monthlySavings = getRecord().getMonthlySavings();

		for (Double snowballFactor : getRecord().getSnowballs()) {
			payOffAmount(Figure.nonMortgageDebt, snowballFactor);
			monthlySavings += snowballFactor;
			getRecord().setFigure(Figure.monthlySavings,
				monthlySavings);
			logger.info("paid debt of $"+nf.format(snowballFactor));
		}
		payOffAmount(Figure.nonMortgageDebt);
	}

	@Override
	public String summarize()
	{
		StringBuilder retval = new StringBuilder();
		retval.append("\n\temergency fund: $"+nf.format(
			getRecord().getFigure(Figure.emergencyFund))+"\n");
		retval.append("\tnon-mortgage-debt: $"+nf.format(
			getRecord().getFigure(Figure.nonMortgageDebt))+"\n");
		retval.append(super.summarize());
		return(""+retval);
	}
}
