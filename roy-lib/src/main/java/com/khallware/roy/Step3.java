/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.util.Date;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class Step3 extends Step
{
	private Logger logger = LoggerFactory.getLogger(Step3.class);
	public static final double PCT_SALARY_EXPENSES = 0.075;
	public static final double MAX_SALARY = 80000.00;

	private boolean redirectFunds = false;

	public Step3()
	{
		super("build the emergency fund (6-12 months of expenses)");
	}

	public boolean satisfied()
	{
		return((amountNeeded() <= 0.01));
	}

	public void calculate() throws FatalException, LifeException
	{
		double numEmergencyMonths =
			getRecord().getFigure(Figure.numEmergencyMonths);
		double amount = amountNeeded();
		double emergencyFund = getRecord().getFigure(
			Figure.emergencyFund);
		double needed = Math.max((amount-getRecord().getBalance()), 0);

		if (numEmergencyMonths < 6.00) {
			throw new FatalException("the number of emergency "
				+"months ("+nf.format(numEmergencyMonths)+") "
				+"must be greater than 6!");
		}
		if (needed > 0.00) {
			Figure[] figures = new Figure[] {
				Figure.currMoCollegeSavings,
				Figure.moRetireSavings
			};
			logger.debug("need to save $"+nf.format(needed));
			redirectFunds = hasAvailableFunds(figures);
			transferToSavings(figures);
			getRecord().setFigure(Figure.temp, needed);
			payOffAmount(Figure.temp, needed);
			getRecord().setFigure(Figure.emergencyFund,
				(amount + emergencyFund));
		}
	}

	public String getNote()
	{
		return((redirectFunds)
			? "utilize monthly retirement and college savings"
			: "utilize only current monthly savings");
	}

	protected double amountNeeded()
	{
		Record rec = getRecord();
		return(Math.max(
			(Math.min(rec.getFigure(Figure.salary), MAX_SALARY)
				* PCT_SALARY_EXPENSES
				* rec.getFigure(Figure.numEmergencyMonths)
				- rec.getFigure(Figure.emergencyFund)
			), 0.00));
	}

	@Override
	public String summarize()
	{
		StringBuilder retval = new StringBuilder();
		Record rec = getRecord();
		retval.append("\n\tnote: "+getNote()+"\n");
		retval.append("\tassumed expenses: "+(100*PCT_SALARY_EXPENSES)
			+"% of $"+Math.min(rec.getFigure(Figure.salary),
				MAX_SALARY)+"\n");
		retval.append("\temergency fund: $"+nf.format(
			rec.getFigure(Figure.emergencyFund))+"\n");
		retval.append(super.summarize());
		return(""+retval);
	}
}
