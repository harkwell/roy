/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import com.khallware.roy.Record.Builder;
import java.util.Collections;
import java.util.Properties;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Date;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.io.FileInputStream;
import java.io.IOException;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class RamseyOnYou
{
	public static final String USAGE = "java RamseyOnYou file.properties";
	public static final int RETIREMENT_AGE = 60;
	public static final double STEP5_COLLEGE_RETURN = 0.12;
	public static final double STEP5_MO_SAVINGS = (2000 / 12);

	private Logger logger = LoggerFactory.getLogger(RamseyOnYou.class);
	private StringBuilder report = new StringBuilder();
	private List<EventHandler> handlers = null;
	private Properties props = null;
	private double nestEgg = 0;
	private List<Step> steps = new ArrayList<>();
	private List<Record> timeline = new ArrayList<>();
	private Record seed = null;

	public RamseyOnYou()
	{
		props = new Properties();
		handlers = new ArrayList<>();
	}

	public RamseyOnYou(Properties props) throws Exception
	{
		this.props = props;
		handlers = Util.parseEventHandlers(props);
	}

	public void exec() throws FatalException, RoYException
	{
		Record record = new Builder(props).build();
		record.validate();
		exec(record);
	}

	public void exec(Record record) throws FatalException, RoYException
	{
		String output = null;
		seed = record;
		handlers.addAll(Util.startCollegeEvents(record.getKids()));
		timeline.add(record);
		steps.add(new Step1());
		steps.add(new Step2());
		steps.add(new Step3());
		steps.add(new Step4());
		steps.add(new Step5());
		steps.add(new Step6());
		steps.add(new Step7());

		MAIN: for (boolean done = false; !done;) {
			for (Step step : steps) {
				step.setFinalRecord(null);
			}
			for (Step step : steps) {
				boolean flag = false;
				try {
					for (EventHandler handler : handlers) {
						step.register(handler);
					}
					step.setTimeline(timeline);

					if (!step.satisfied()) {
						output = ""+step.getRecord();
						logger.info(output);
						report.append(output);
						step.exec();
						output = ""+step;
						output += step.summarize();
						logger.info("\n"+output);
						report.append(output);
					}
					else {
						Class clazz = step.getClass();
						output = "\nFULFILLED: "
							+clazz.getSimpleName()
							+"\n";
						logger.info(output);
						report.append(output);
						step.setFinalRecord(
							step.getRecord());
					}
				}
				catch (LifeException e) {
					Date d = e.getTranspiredDate();
					step.callHandlers(d);
					step.setToday(Util.dayAfter(d));
					logger.info(""+e);
					report.append(""+e);
					flag = true;
				}
				step.enforceRetirement(step.getToday());
				step.enforceDeath(step.getToday());
				Util.removeExpiredEventHandlers(handlers,
					step.getToday());

				if (flag) {
					continue MAIN;
				}
			}
			done = true;
		}
	}

	public Record getSeedRecord()
	{
		return(seed);
	}

	public List<Record> getTimeline()
	{
		return(timeline);
	}

	public List<EventHandler> getEventHandlers()
	{
		return(handlers);
	}

	public String report()
	{
		return(""+report);
	}

	public List<Step> getSteps()
	{
		return(steps);
	}

	public static void main(String[] args) throws Exception
	{
		if (args.length < 1) {
			throw new IllegalArgumentException(USAGE);
		}
		Properties props = new Properties();
		props.load(new FileInputStream(args[0]));
		new RamseyOnYou(props).exec();
	}
}
