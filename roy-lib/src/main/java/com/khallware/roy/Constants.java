/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

public class Constants
{
	public static final String PROPHEAD = "roy.";
	public static final String DATE_FORMAT = "yyyyMMdd";
	public static final String MONEY_FORMAT = "#,##0.00;(#)";
	public static final String NUMBER_FORMAT = "#,##0.000000;(#)";
	public static final long SECS_PER_MO = (60 * 60 * 24 * (365 / 12));
	public static final int DEATH_AGE = 100;
}
