/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.util.Date;

public class RetirementException extends LifeException
{
	private static final long serialVersionUID = 0x0001L;

	public RetirementException(Date transpiredDate)
	{
		super(transpiredDate);
	}

	public RetirementException(Date transpiredDate, String message)
	{
		super(transpiredDate, message);
	}
}
