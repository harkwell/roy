/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

public class FatalException extends Exception
{
	private static final long serialVersionUID = 0x0001L;

	public FatalException() { }

	public FatalException(String message)
	{
		super(message);
	}

	public FatalException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public FatalException(Throwable cause)
	{
		super(cause);
	}
}
