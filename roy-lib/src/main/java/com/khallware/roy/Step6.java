/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.util.Date;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class Step6 extends Step
{
	private Logger logger = LoggerFactory.getLogger(Step6.class);

	public Step6()
	{
		super("pay off the mortgage");
	}

	public boolean satisfied()
	{
		return((getRecord().getFigure(Figure.mortgageDebt) < 0.01));
	}

	public void calculate() throws FatalException, LifeException
	{
		while (!satisfied()) {
			Record rec = advanceMonth();
			double loanValue = rec.getFigure(Figure.mortgageDebt);
			logger.debug("paying $"+nf.format(rec.getBalance())
				+" to mortgage loan $"+nf.format(loanValue));
			rec.setFigure(Figure.mortgageDebt,
				(loanValue - rec.getBalance()));
			rec.setBalance(0.00);
			enforceDeath(getToday());
			enforceRetirement(getToday());
		}
	}

	@Override
	public String summarize()
	{
		StringBuilder retval = new StringBuilder();
		retval.append("\n"+super.summarize());
		return(""+retval);
	}
}
