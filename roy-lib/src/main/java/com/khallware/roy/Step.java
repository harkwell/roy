/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import com.khallware.roy.EventHandler.Type;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Collections;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public abstract class Step
{
	private static Logger logger = LoggerFactory.getLogger(Step.class);

	protected NumberFormat nf = new DecimalFormat(Constants.MONEY_FORMAT);
	protected DateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
	protected List<EventHandler> handlers = null;
	protected List<Record> timeline = null;
	protected String description = null;
	protected Record finalRecord = null;

	public Step(String description)
	{
		this.description = description;
		handlers = new ArrayList<>();
	}

	public abstract boolean satisfied();
	public abstract void calculate() throws FatalException, LifeException;

	public void register(EventHandler handler)
	{
		if (!handlers.contains(handler)) {
			handlers.add(handler);
		}
	}

	public void exec() throws FatalException, LifeException
	{
		calculate();
		finalRecord = getRecord();
	}

	public String summarize()
	{
		StringBuilder retval = new StringBuilder();
		Record rec = getRecord();
		retval.append("\tstep finished: "+df.format(
			rec.getDate(Event.currentDate))+"\n");
		retval.append("\tcapital: $"+nf.format(rec.getBalance())+"\n");
		retval.append("\tmonthly savings: $"+nf.format(
			rec.getFigure(Figure.monthlySavings))+"\n");
		retval.append("\tbalance: $"+nf.format(Util.networth(rec)));
		return(""+retval);
	}

	public void setTimeline(List<Record> timeline)
	{
		this.timeline = (timeline == null)
			? new ArrayList<Record>()
			: timeline;
	}

	public List<Record> getTimeline()
	{
		if (timeline == null) {
			setTimeline(null);
		}
		return(timeline);
	}

	protected void payOffAmount(Figure figure) throws LifeException,
			FatalException
	{
		payOffAmount(figure, getRecord().getFigure(figure));
	}

	protected void payOffAmount(Figure figure, Double amount)
			throws LifeException, FatalException
	{
		Record rec = getRecord();
		Date today = rec.getDate(Event.currentDate);
		double available = 0.00;
		double owed = amount;
		double paid = 0.00;
		enforceHasSavings();

		if (owed < 0.01 || owed > rec.getFigure(figure)) {
			throw new FatalException("payoff amount for figure "
				+"("+figure+") is more than what is owed");
		}
		logger.debug("paying off $"+nf.format(amount));

		while (owed >= 0.01) {
			rec = advanceMonth();
			available = rec.getBalance();
			today = rec.getDate(Event.currentDate);

			if ((owed - available) < 0.01) {
				rec.setBalance((available - owed));
				paid = owed;
				owed = 0.00;
			}
			else {
				rec.setBalance(0.00);
				paid = available;
				owed -= available;
			}
			rec.setFigure(figure, (rec.getFigure(figure) - paid));
			enforceDeath(today);
			enforceRetirement(today);

			if (Util.nextEventDate(handlers, today) != null) {
				throw new LifeException(today);
			}
		}
	}

	public Record advanceMonth() throws FatalException
	{
		Record retval = null;
		try {
			retval = (Record)getRecord().clone();
			retval.incrementOrdinal();
		}
		catch (CloneNotSupportedException e) {
			throw new FatalException(""+e, e);
		}
		double[] pandi = Util.mortgatePrincipalInterestPayment(retval);
		Date today = retval.getDate(Event.currentDate);
		retval.setDate(Event.currentDate, Util.monthAfter(today));

		for (Figure figure : Figure.values()) {
			double val = retval.getFigure(figure);
			double tmp = 0.00;
			//logger.debug("figure/val:"+figure+"="+nf.format(val));

			switch (figure) {
			case monthlySavings:
				tmp = retval.getBalance();
				retval.setBalance((val + tmp));
				break;
			case mortgageDebt:
				retval.setFigure(figure, (val - pandi[0]));
				break;
			case collegeBalance:
			case currRetireSavings:
				tmp = (retval.getFigure(Figure.marketRate)/12);
				retval.setFigure(figure, (val + (val * tmp)));
				break;
			case savingsInt:
				tmp = retval.getFigure(Figure.capital);
				retval.setFigure(Figure.capital,
					(tmp + (tmp * (val / 12))));
				tmp = retval.getFigure(Figure.emergencyFund);
				retval.setFigure(Figure.emergencyFund,
					(tmp + (tmp * (val / 12))));
				break;
			case nestEgg:
				tmp = retval.getFigure(Figure.marketRate);
				retval.setFigure(figure,
					(val + (val * (tmp / 12))));
				break;
			}
		}
		getTimeline().add(retval);
		return(retval);
	}

	public void setToday(Date date)
	{
		getRecord().setDate(Event.currentDate, date);
	}

	public Date getToday()
	{
		return(getRecord().getDate(Event.currentDate));
	}

	public boolean completed()
	{
		return(finalRecord != null);
	}

	public void setFinalRecord(Record record)
	{
		finalRecord = record;
	}

	public Record getRecord()
	{
		Record retval = (finalRecord != null)
			? finalRecord
			: getTimeline().get(getTimeline().size() - 1);
		return(retval);
	}

	public boolean hasAvailableFunds(Figure[] figures)
	{
		boolean retval = false;

		for (Figure figure : figures) {
			retval |= (getRecord().getFigure(figure) > 0.00);
		}
		return(retval);
	}

	public double transferToSavings(Figure[] figures)
	{
		double retval = 0.0;
		Record rec = getRecord();
		double monthlySavings = rec.getFigure(Figure.monthlySavings);

		for (Figure figure : figures) {
			monthlySavings += rec.getFigure(figure);
			rec.setFigure(figure, 0.00);
		}
		rec.setFigure(Figure.monthlySavings, monthlySavings);
		retval = monthlySavings;
		return(retval);
	}

	public void enforceHasSavings() throws FatalException
	{
		double savings = getRecord().getFigure(Figure.monthlySavings);

		if (savings <= 0.00) {
			throw new FatalException("there is not enough monthly "
				+"savings to continue");
		}
	}

	public void enforceRetirement(Date date) throws LifeException
	{
		Date retirement = getRecord().getDate(Event.retirement);

		if (date.after(retirement)) {
			throw new RetirementException(date, ""+this+" happens "
				+"after retirement");
		}
	}

	public void enforceDeath(Date date) throws LifeException
	{
		Date death = getRecord().getDate(Event.death);

		if (date.after(death)) {
			throw new MortalityException(date, ""+this+" happens "
				+"postmortem");
		}
	}

	@Deprecated
	public int calcDurationMonths(double needed)
	{
		int retval = 0;
		double monthlySavings = getRecord().getFigure(
			Figure.monthlySavings);

		if (needed > 0.00) {
			retval = (int)((needed / monthlySavings) + 1);
		}
		return(retval);
	}

	@Deprecated
	public long calcDuration(double needed)
	{
		double monthlySavings = getRecord().getFigure(
			Figure.monthlySavings);
		long retval = (long)((needed / monthlySavings)
			* (Record.SECS_PER_MO * 1000));
		return(retval);
	}

	public Date increaseSavings(double amount) throws LifeException,
			FatalException
	{
		return(increaseSavings(amount, true));
	}

	public Date increaseSavings(double amount, boolean doEnforceRetirement)
			throws LifeException, FatalException
	{
		Date retval = getRecord().getDate(Event.currentDate);

		while (getRecord().getBalance() < amount) {
			advanceMonth();
			retval = getRecord().getDate(Event.currentDate);
			enforceDeath(retval);

			if (doEnforceRetirement) {
				enforceRetirement(retval);
			}
		}
		return(retval);
	}

	public void callHandlers(Date date)
	{
		List<EventHandler> toRemove = new ArrayList<>();

		for (EventHandler handler : handlers) {
			if (handler.isReady(date)) {
				logger.info("calling event handler on date "
					+df.format(date));
				handler.handle(getRecord());

				if (handler.isSingleInvocation()) {
					toRemove.add(handler);
				}
			}
		}
		for (EventHandler handler : toRemove) {
			handlers.remove(handler);
		}
	}

	protected boolean childUnderEighteen(Date bday, Date givenDate)
	{
		boolean retval = false;
		double age = Util.differenceYears(givenDate, bday);
		retval = (age < 18.00);
		// logger.debug("child born on "+df.format(bday)+" is "
		// 	+((retval) ? "" : "not ")+"under eighteen on "
		// 	+df.format(givenDate));
		return(retval);
	}

	protected Date childTurnsEighteen(Date bday)
	{
		return(new Date(bday.getTime() + (18 * 12 * Record.SECS_PER_MO
			* 1000)));
	}

	protected boolean childUnderEighteen(Date bday)
	{
		return(childUnderEighteen(bday,
			getRecord().getDate(Event.currentDate)));
	}

	protected List<Date> findKidsInCollege(Date future)
	{
		List<Date> retval = new ArrayList<>();

		for (Date bday : getRecord().getKids()) {
			if (childUnderEighteen(bday, future)) {
				retval.add(bday);
			}
		}
		return(retval);
	}

	@Override
	public String toString()
	{
		return(this.getClass().getSimpleName()+": "+description);
	}
}
