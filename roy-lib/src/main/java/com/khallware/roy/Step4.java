/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.util.Date;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class Step4 extends Step
{
	public static final double PERCENTAGE = 0.15;
	private Logger logger = LoggerFactory.getLogger(Step4.class);

	private boolean redirectFunds = false;

	public Step4()
	{
		super("maximize retirement ("+(PERCENTAGE * 100)+"% "
			+"of gross income)");
	}

	public boolean satisfied()
	{
		return((amountNeeded() <= 0.01));
	}

	public void calculate() throws FatalException, LifeException
	{
		Record rec = getRecord();
		double yearlyGross = rec.getFigure(Figure.salary);
		double amount = amountNeeded();
		double moRetireSavings = rec.getFigure(Figure.moRetireSavings);
		double monthlySavings = rec.getFigure(Figure.monthlySavings);
		double needed = Math.max(amount - moRetireSavings, 0);

		if (needed > 0.00) {
			Figure[] figures = new Figure[] {
				Figure.currMoCollegeSavings
			};
			redirectFunds = hasAvailableFunds(figures);
			transferToSavings(figures);
			moRetireSavings = rec.getFigure(Figure.moRetireSavings);
			monthlySavings -= needed;
			moRetireSavings += needed;

			if (monthlySavings < 0) {
				throw new FatalException("not enough funds to "
					+"save "+(100 * PERCENTAGE)+"% of "
					+"$"+nf.format(yearlyGross)+" (need "
					+nf.format(needed)+" more)");
			}
			rec.setFigure(Figure.monthlySavings, monthlySavings);
			rec.setFigure(Figure.moRetireSavings, moRetireSavings);
		}
	}

	public String getNote()
	{
		return((redirectFunds)
			? "utilize monthly retirement and college savings"
			: "utilize only current monthly savings");
	}

	protected double amountNeeded()
	{
		return(((PERCENTAGE*getRecord().getFigure(Figure.salary))/12));
	}

	@Override
	public String summarize()
	{
		StringBuilder retval = new StringBuilder();
		Record rec = getRecord();
		retval.append("\n\tnote: "+getNote()+"\n");
		retval.append("\tmonthly retirement savings: $"+nf.format(
			rec.getFigure(Figure.moRetireSavings))+"\n");
		retval.append("\tmonthly college savings: $"+nf.format(
			rec.getFigure(Figure.currMoCollegeSavings))+"\n");
		retval.append("\temergency fund: $"+nf.format(
			rec.getFigure(Figure.emergencyFund))+"\n");
		retval.append(super.summarize());
		return(""+retval);
	}
}
