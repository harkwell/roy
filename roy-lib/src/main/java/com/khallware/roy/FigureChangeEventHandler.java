/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.util.Date;

public class FigureChangeEventHandler implements EventHandler
{
	Type type = null;
	Date date = null;
	Double amount = null;
	Figure figure = null;
	boolean accumulate = false;

	public FigureChangeEventHandler(Type type, Figure figure, Date date,
			Double amount)
	{
		this.type = type;
		this.date = date;
		this.amount = amount;
		this.figure = figure;
		this.accumulate = false;
	}

	public FigureChangeEventHandler(Type type, Figure figure, Date date,
			Double amount, boolean accumulate)
	{
		this.type = type;
		this.date = date;
		this.amount = amount;
		this.figure = figure;
		this.accumulate = accumulate;
	}

	public void handle(Record record)
	{
		double currValue = record.getFigure(figure);
		double newValue = ((accumulate) ? amount + currValue : amount);
		newValue = Math.max(newValue, 0.00);
		record.setFigure(figure, newValue);
	}

	public boolean isReady(Date date)
	{
		return(date.equals(this.date) || date.after(this.date));
	}

	public boolean isSingleInvocation()
	{
		return(true);
	}

	public Date getDate()
	{
		return(date);
	}

	public Type getType()
	{
		return(type);
	}

	public Double getAmount()
	{
		return(amount);
	}

	public Figure getFigure()
	{
		return(figure);
	}

	@Override
	public String toString()
	{
		return(new StringBuilder()
			.append(this.getClass().getName()+" ")
			.append("type=\""+getType()+"\" ")
			.append("figure=\""+getFigure()+"\" ")
			.append("date=\""+getDate()+"\" ")
			.append("amount=\""+getAmount()+"\" ")
			.toString());
	}
}
