/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

public class RoYException extends Exception
{
	private static final long serialVersionUID = 0x0001L;

	public RoYException()
	{
	}

	public RoYException(String message)
	{
		super(message);
	}

	public RoYException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public RoYException(Throwable cause)
	{
		super(cause);
	}
}
