/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import com.khallware.roy.EventHandler.Type;
import java.util.regex.Pattern;
import java.util.List;
import java.util.Date;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Properties;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class Util
{
	private static Logger logger = LoggerFactory.getLogger(Util.class);

	public static double networth(Record rec)
	{
		double debts = (rec.getFigure(Figure.nonMortgageDebt)
			+ rec.getFigure(Figure.mortgageDebt));
		double assets = (rec.getFigure(Figure.emergencyFund)
			+ rec.getFigure(Figure.currRetireSavings)
			+ rec.getFigure(Figure.capital)
			+ rec.getFigure(Figure.nestEgg)
			+ rec.getFigure(Figure.houseValue)
			+ rec.getFigure(Figure.collegeBalance));
		return((assets - debts));
	}

	public static List<EventHandler> parseEventHandlers(Properties props)
			throws ParseException, FatalException
	{
		List<EventHandler> retval = new ArrayList<>();
		Pattern pattern = Pattern.compile(Constants.PROPHEAD+"event.*");
		DateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
		NumberFormat nf = new DecimalFormat(Constants.NUMBER_FORMAT);
		EventHandler handler = null;
		String[] dat = null;
		int offset = 0;

		for (Object key : props.keySet()) {
			if (!pattern.matcher(""+key).matches()) {
				continue;
			}
			dat = (""+key).split("\\.");
			offset = (Constants.PROPHEAD).split("\\.").length;

			if (!dat[offset].equals("event")) {
				continue;
			}
			logger.debug("found event: "+key+"="+props.get(""+key));
			final Date date = df.parse(dat[offset+1]);
			final Type type = Type.valueOf(dat[offset+2]);
			final Double amount = nf.parse(""+props.get(""
				+key)).doubleValue();
			
			switch(type) {
			case raise:
				handler = new FigureChangeEventHandler(
					type, Figure.salary, date, amount);
				retval.add(handler);
				break;
			case mortgage:
				if (dat.length < offset+3) {
					throw new FatalException("invalid "
						+"property specified for "
						+"mortgate");
				}
				if ("loan".equals(dat[offset+2])) {
					// KDH calculate effect on mo savings
					handler = new FigureChangeEventHandler(
						type, Figure.mortgageDebt, date,
						amount);
					retval.add(handler);
				}
				if ("housevalue".equals(dat[offset+2])) {
					handler = new FigureChangeEventHandler(
						type, Figure.houseValue, date,
						amount);
					retval.add(handler);
				}
				break;
			case savings:
				handler = new FigureChangeEventHandler(type,
					Figure.monthlySavings, date, amount);
				retval.add(handler);
				break;
			default:
				throw new FatalException("unhandled "
					+"EventHandler type: "+type);
			}
		}
		return(retval);
	}

	public static void removeExpiredEventHandlers(List<EventHandler> list,
			Date expiration)
	{
		List<EventHandler> toRemove = new ArrayList<>();

		for (EventHandler handler : list) {
			if (handler.getDate().before(expiration)) {
				toRemove.add(handler);
			}
		}
		for (EventHandler handler : toRemove) {
			list.remove(handler);
		}
	}

	public static Date nextEventDate(List<EventHandler> handlers,
			Date expiration)
	{
		Date retval = null;
		Date nextEvent = nextEventDate(handlers);

		if (expiration == null || (nextEvent != null
				&& nextEvent.before(expiration))) {
			retval = nextEvent;
		}
		return(retval);
	}

	public static Date nextEventDate(List<EventHandler> handlers)
	{
		Date retval = null;

		for (EventHandler handler : handlers) {
			if (retval == null) {
				retval = handler.getDate();
			}
			else if (handler.getDate().before(retval)) {
				retval = handler.getDate();
			}
		}
		return(retval);
	}

	public static Date dayBefore(Date date)
	{
		return(new Date(date.getTime() - (24 * 60 * 60 * 1000)));
	}

	public static Date dayAfter(Date date)
	{
		return(new Date(date.getTime() + (24 * 60 * 60 * 1000)));
	}

	public static Date monthBefore(Date date)
	{
		return(new Date(date.getTime() - (Record.SECS_PER_MO * 1000)));
	}

	public static Date monthAfter(Date date)
	{
		return(new Date(date.getTime() + (Record.SECS_PER_MO * 1000)));
	}

	public static Date yearsAfter(int years, Date date)
	{
		return(new Date(date.getTime() + (years * 12
			* Record.SECS_PER_MO * 1000)));
	}

	@Deprecated
	public static List<EventHandler> startCollegeEvents(List<Date> bdays)
	{
		List<EventHandler> retval = new ArrayList<>();

		for (Date bday : bdays) {
			Date startCollegeDate = new Date(bday.getTime()
				+ (18 * 12 * Record.SECS_PER_MO * 1000));
			double amount = (2000.00 / 12);
			EventHandler handler = new FigureChangeEventHandler(
				Type.adjustment, Figure.monthlySavings,
				startCollegeDate, amount, true);
			retval.add(handler);
			handler = new FigureChangeEventHandler(
				Type.adjustment, Figure.currMoCollegeSavings,
				startCollegeDate, (-1 * amount), true);
		}
		return(retval);
	}

	protected Date childTurnsEighteen(Date bday)
	{
		return(new Date(bday.getTime() + (18 * 12 * Record.SECS_PER_MO
			* 1000)));
	}

	public static double differenceMonths(Date later, Date earlier)
	{
		double millis = (later.getTime() - earlier.getTime());
		return(millis / (1000 * Record.SECS_PER_MO));
	}

	public static double differenceYears(Date later, Date earlier)
	{
		double retval = 0.00;
		double millis = (later.getTime() - earlier.getTime());
		retval = (millis / (1000 * Record.SECS_PER_MO * 12));
		return(retval);
	}

	public static double[] mortgatePrincipalInterestPayment(Record rec)
	{
		double[] retval = new double[] { 0.00, 0.00 };
		double loanValue = rec.getFigure(Figure.mortgageDebt);
		double periodInt = (rec.getFigure(Figure.mortgageInt) / 12);
		int numPayments = (int)(rec.getFigure(Figure.mortgageLength)
			* 12);
		double payment = (loanValue * ((periodInt
			* Math.pow((1 + periodInt), numPayments))
			/ (Math.pow((1 + periodInt), numPayments) - 1)));
		double interestDue = (loanValue * periodInt);
		double PrincipalDue = (payment - interestDue);
		/* logger.debug("loanValue="+loanValue);
		logger.debug("periodInt="+periodInt);
		logger.debug("numPayments="+numPayments);
		logger.debug("payment="+payment);
		logger.debug("interestDue="+interestDue);
		logger.debug("PrincipalDue="+PrincipalDue); */
		retval[0] = PrincipalDue;
		retval[1] = interestDue;
		logger.debug("principal and interest = "
			+Arrays.toString(retval));
		return(retval);
	}

	public static double annualCompoundInterest(double principal,
			double rate, int years)
	{
		return(compoundInterest(principal, rate, years, 1));
	}

	public static double compoundInterest(double principal, double rate,
			int years, int numTimes)
	{
		double retval = (principal * Math.pow((1 + (rate / numTimes)),
			years));
		return(retval);
	}

	/**
	 * Compound interest calculation with regular payments.
	 * returns future value
	 * value = current value
	 * payment = regulary monthly deposit amount (must be a positive number)
	 * rate = the annual interest rate
	 * years = the length of time to calculate value for
	 * numTimes = number of compounds per year (usually annually or monthly)
	 */
	public static double compoundInterestWithPayments(double value,
			double payment, double rate, int years, int numTimes)
	{
		double retval = capitalAccumulation((rate / 12), (years * 12),
			value) + futureValueOfSeries(payment, (years * 12),
			(rate / 12));
		return(retval);
	}

	/**
	 * FV = ((1 + i)^n * PV)
	 *
	 * FV = capital accumulation
	 * i = interest rate per period
	 * n = number of periods
	 * PV = present value
	 */
	public static double capitalAccumulation(double i, int n, double PV)
	{
		return((Math.pow((1 + i), n) * PV));
	}

	/**
	 * FV = PMT * (((1 + i)^n -1) / i)
	 *
	 * FV = future value of a series
	 * PMT = periodic payment amount
	 * n = number of periods
	 * i = interest rate per period
	 */
	public static double futureValueOfSeries(double PMT, int n, double i)
	{
		return((PMT * ((Math.pow((1 + i), n) - 1) / i)));
	}

	/**
	 * How many years will it take to pay off given amount?
	 * value = current value of the object (equity)
	 * amount = the future value of the object (debt)
	 * payment = the monthly payment
	 * rate = the interest rate on the loan
	 */
	public static int compoundInterestWithPaymentsYears(double value,
			double amount, double payment, double rate)
	{
		int retval = 1;

		for (double rslt = 0.00; (rslt = compoundInterestWithPayments(
			value, payment, rate, retval, 1)) < amount; retval++) {}
		return(retval);
	}

	public static double compoundInterestTime(double amount,
			double principal, double rate, int numTimes)
	{
		double retval = ((Math.log(amount / principal)
			/ Math.log((1 + (rate / numTimes)))) / numTimes);
		logger.debug("t = (ln(A/P) / ln(1+r/n)) / n)");
		logger.debug("t = "+retval);
		logger.debug("A = "+amount);
		logger.debug("P = "+principal);
		logger.debug("r = "+rate);
		logger.debug("n = "+numTimes);
		return(retval);
	}
}
