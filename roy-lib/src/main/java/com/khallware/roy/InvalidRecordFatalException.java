/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

public class InvalidRecordFatalException extends FatalException
{
	public static enum Reason { snowball, kid, figure, event, unknown };

	private Reason reason = Reason.unknown;
	private Figure figure = null;
	private Event event = null;

	public InvalidRecordFatalException(Enum item)
	{
		super("invalid record item: \""+item+"\"");
		try {
			figure = Figure.valueOf(""+item);
			reason = Reason.figure;
		}
		catch (IllegalArgumentException e1) {
			try {
				event = Event.valueOf(""+item);
				reason = Reason.event;
			}
			catch (IllegalArgumentException e2) {
				reason = Reason.valueOf(""+item);
			}
		}
	}

	public InvalidRecordFatalException(String message)
	{
		super(message);
	}

	public InvalidRecordFatalException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public InvalidRecordFatalException(Throwable cause)
	{
		super(cause);
	}

	public Figure getFigure()
	{
		return(figure);
	}

	public Event getEvent()
	{
		return(event);
	}

	public Reason getReason()
	{
		return(reason);
	}
}
