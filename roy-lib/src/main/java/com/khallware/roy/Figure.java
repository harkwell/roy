/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

public enum Figure
{
	/** LIABILITIES **/
	mortgageDebt,                // total amount of mortgage debt (payoff)
	nonMortgageDebt,             // total amount of debt that isn't mortgage

	/** ASSETS **/
	currRetireSavings,           // total current retirement savings amount
	collegeBalance,              // total amount of money saved for college
	emergencyFund,               // the amount of money in emergency fund
	capital,                     // available capital (less emgcy, edu)

	/** OTHER **/
	temp,                        // useful for calculations
	salary,                      // yearly gross salary
	houseValue,                  // value of the house
	marketRate,                  // rate of return on market investments
	savingsInt,                  // interest rate for bank savings account
	mortgageInt,                 // interest rate for the mortgage (APR)
	mortgageLoan,                // original loan amount
	mortgageLength,              // length (in years) for the mortgage
	mortgagePayment,             // monthly principal and interest payment
	currMoCollegeSavings,        // current monthly college savings
	numEmergencyMonths,          // number of months required for emergency
	moRetireSavings,             // current monthly retirement savings
	monthlySavings,              // amount of money saved per month
	nestEgg;                     // to calculate wealth building
}
