/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.util.Date;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class Step5 extends Step
{
	public static final double RETURN_RATE = 0.15;
	public static final int YEARLY_SAVINGS = 2000;
	public static final int SCHOOL_AGE = 18;

	private Logger logger = LoggerFactory.getLogger(Step5.class);

	public Step5()
	{
		super("save for college ($"+YEARLY_SAVINGS
			+" per child per year until "+SCHOOL_AGE+")");
	}

	public boolean satisfied()
	{
		boolean retval = false;
		try {
			retval = ((missingCollegeFunds(
				getRecord().getFigure(Figure.collegeBalance))
				<= 0.01));
		}
		catch (LifeException e) {
			retval = true;
		}
		return(retval);
	}

	public double missingCollegeFunds(double egg) throws LifeException
	{
		double retval = (0.00 - egg);

		for (Date bday : getRecord().getKids()) {
			retval += collegeFundNeeded(bday);
		}
		enforcePreCollegeAges();
		return(retval);
	}

	public void calculate() throws FatalException, LifeException
	{
		Record rec = getRecord();
		double collegeBalance = rec.getFigure(Figure.collegeBalance);
		double needed = missingCollegeFunds(collegeBalance);
		double goal = ((YEARLY_SAVINGS / 12) * rec.getKids().size());

		while (needed > 0.01) {
			rec.setFigure(Figure.temp, needed);
			payOffAmount(Figure.temp, needed);
			rec = getRecord();
			collegeBalance += needed;
			rec.setFigure(Figure.collegeBalance, collegeBalance);
			needed = missingCollegeFunds(collegeBalance);
		}
		logger.debug("now take from mo savings: $"+nf.format(goal));
		rec.setFigure(Figure.monthlySavings,
			(rec.getFigure(Figure.monthlySavings) - goal));
		enforceHasSavings();
		rec.setFigure(Figure.collegeBalance, collegeBalance);
		rec.setFigure(Figure.currMoCollegeSavings, goal);
	}

	@Override
	public String summarize()
	{
		StringBuilder retval = new StringBuilder();
		Record rec = getRecord();
		double egg = rec.getFigure(Figure.collegeBalance);
		retval.append("\n\tdo not rely on social security benefits\n");
		retval.append("\tcollege nest egg: $"+nf.format(egg)+"\n");
		retval.append("\temergency fund: $"+nf.format(
			rec.getFigure(Figure.emergencyFund))+"\n");
		retval.append(super.summarize());
		return(""+retval);
	}

	private double collegeFundNeeded(Date bday)
	{
		Date now = getRecord().getDate(Event.currentDate);
		double retval = Util.annualCompoundInterest(YEARLY_SAVINGS,
			RETURN_RATE, (int)Util.differenceYears(now, bday));
		logger.debug("college fund has accrued to "
			+"$"+nf.format(retval)+" from "+df.format(bday)
			+" until now ("+df.format(now)+")");
		return(retval);
	}

	private void enforcePreCollegeAges() throws LifeException
	{
		Date now = getRecord().getDate(Event.currentDate);

		for (Date bday : getRecord().getKids()) {
			if (!childUnderEighteen(bday)) {
				throw new LifeException(now,
					"child reached college age before "
					+"enough savings were made.");
			}
		}
	}
}
