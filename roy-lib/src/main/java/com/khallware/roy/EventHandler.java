/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.util.Date;

public interface EventHandler
{
	public enum Type { raise, gift, mortgage, debt, savings, inheritance,
		adjustment };

	public void handle(Record record);
	public boolean isReady(Date date);
	public boolean isSingleInvocation();
	public Date getDate();
	public Type getType();
}
