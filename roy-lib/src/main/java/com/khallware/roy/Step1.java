/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.util.Date;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class Step1 extends Step
{
	public static final double SAVE_AMOUNT_NOW = 1000;

	private Logger logger = LoggerFactory.getLogger(Step1.class);
	private boolean redirectFunds = false;

	public Step1()
	{
		super("save $"+SAVE_AMOUNT_NOW+" now!");
	}

	public boolean satisfied()
	{
		return((getRecord().getFigure(Figure.emergencyFund)
			>= SAVE_AMOUNT_NOW));
	}

	public void calculate() throws FatalException, LifeException
	{
		Record rec = null;
		double needed = 0.00;

		while (!satisfied()) {
			rec = advanceMonth();
			needed = (SAVE_AMOUNT_NOW
				- rec.getFigure(Figure.emergencyFund));

			if (rec.getBalance() > needed) {
				rec.setFigure(Figure.emergencyFund,
					SAVE_AMOUNT_NOW);
				rec.setBalance(rec.getBalance() - needed);
			}
			else {
				rec.setFigure(Figure.emergencyFund,
					rec.getBalance());
				rec.setBalance(0.00);
			}
			enforceDeath(getToday());
			enforceRetirement(getToday());
		}
	}

	public String getNote()
	{
		String n = "sell the car if you have an expensive one\n\t";
		return((redirectFunds)
			? n+"utilizing monthly retirement and college savings"
			: n+"utilizing the current monthly savings");
	}

	@Override
	public String summarize()
	{
		StringBuilder retval = new StringBuilder();
		retval.append("\n\tnote: "+getNote()+"\n");
		retval.append("\temergency fund: $"+nf.format(
			getRecord().getFigure(Figure.emergencyFund))+"\n");
		retval.append("\tnon-mortgage-debt: $"+nf.format(
			getRecord().getFigure(Figure.nonMortgageDebt))+"\n");
		retval.append(super.summarize());
		return(""+retval);
	}
}
