/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class LifeException extends RoYException
{
	private static final long serialVersionUID = 0x0001L;
	private DateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);

	private Date transpiredDate = null;

	public LifeException(Date transpiredDate)
	{
		this.transpiredDate = transpiredDate; 
	}

	public LifeException(Date transpiredDate, String message)
	{
		super(message);
		this.transpiredDate = transpiredDate; 
	}

	public Date getTranspiredDate()
	{
		return(transpiredDate);
	}

	@Override
	public String toString()
	{
		return("Transpired date: "+df.format(getTranspiredDate())+"\n"
			+super.toString());
	}
}
