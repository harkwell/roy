/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.util.Date;
import java.util.Calendar;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class Step7 extends Step
{
	public static final long SECS_PER_MO = Record.SECS_PER_MO;
	public static final double INFLATION = 0.08;
	public static final double RETURN_RATE = 0.15;

	private Logger logger = LoggerFactory.getLogger(Step7.class);
	private Calendar cal = Calendar.getInstance();

	public Step7()
	{
		super("build wealth");
	}

	public boolean satisfied()
	{
		Record rec = getRecord();
		Date retirement = rec.getDate(Event.retirement);
		double freedom = (rec.getFigure(Figure.salary) / INFLATION);
		return((getToday().after(retirement)
			|| (rec.getFigure(Figure.nestEgg) >= freedom)));
	}

	public void calculate() throws FatalException, LifeException
	{
		Record rec = getRecord();
		double freedom = (rec.getFigure(Figure.salary) / INFLATION);
		logger.debug("freedom comes at $"+nf.format(freedom));

		for (double nestEgg = 0.00; !satisfied();) {
			rec = advanceMonth();
			nestEgg = rec.getFigure(Figure.nestEgg);
			rec.setFigure(Figure.nestEgg,
				(nestEgg + rec.getBalance()));
			rec.setBalance(0.00);
			enforceDeath(getToday());
			enforceRetirement(getToday());
		}
	}

	@Override
	public String summarize()
	{
		StringBuilder retval = new StringBuilder();
		double nestEgg = getRecord().getFigure(Figure.nestEgg);
		double earnings = (nestEgg * RETURN_RATE);
		retval.append("\n\tmoney now earns (per year) "
			+"$"+nf.format(earnings)+"\n");
		retval.append("\tnest egg: $"+nf.format(nestEgg)+"\n");
		retval.append(super.summarize());
		return(""+retval);
	}
}
