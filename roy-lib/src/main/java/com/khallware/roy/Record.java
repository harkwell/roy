/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import com.khallware.roy.InvalidRecordFatalException.Reason;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.HashMap;
import java.util.Calendar;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Collections;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class Record implements Cloneable, Serializable
{
	public static final String PROPHEAD = Constants.PROPHEAD;
	public static final long SECS_PER_MO = Constants.SECS_PER_MO;
	public static final int DEATH_AGE = Constants.DEATH_AGE;

	private static Logger logger = LoggerFactory.getLogger(Record.class);
	protected static Map<String, Figure> figureProps = null;
	protected static Map<String, Event> eventProps = null;

	protected NumberFormat nf = new DecimalFormat(Constants.MONEY_FORMAT);
	protected DateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
	protected Calendar cal = Calendar.getInstance();
	protected Map<Figure, Double> figures = null;
	protected Map<Event, Date> dates = null;
	protected List<Double> snowballs = null;
	protected List<Date> kids = null;
	protected int ordinal = 0;

	static {
		figureProps = new HashMap<String, Figure>() {{
			this.put("balance.401k", Figure.currRetireSavings);
			this.put("balance.college", Figure.collegeBalance);
			this.put("balance.emergency", Figure.emergencyFund);
			this.put("balance.capital", Figure.capital);
			this.put("debt.mortgage", Figure.mortgageDebt);
			this.put("debt.nonmortgage", Figure.nonMortgageDebt);
			this.put("monthly.401k", Figure.moRetireSavings);
			this.put("monthly.college",Figure.currMoCollegeSavings);
			this.put("monthly.savings", Figure.monthlySavings);
			this.put("months.emergency", Figure.numEmergencyMonths);
			this.put("mortgage.apr", Figure.mortgageInt);
			this.put("mortgage.housevalue", Figure.houseValue);
			this.put("mortgage.loan", Figure.mortgageLoan);
			this.put("mortgage.length", Figure.mortgageLength);
			this.put("mortgage.payment", Figure.mortgagePayment);
			this.put("rate.bank", Figure.savingsInt);
			this.put("rate.market", Figure.marketRate);
			this.put("salary", Figure.salary);
		}};
		eventProps = new HashMap<String, Event>() {{
			this.put("date.birth", Event.birth);
			this.put("date.death", Event.death);
			this.put("date.retirement", Event.retirement);
			this.put("date.today", Event.currentDate);
		}};
	}

	public static class Builder
	{
		private DateFormat _df = new SimpleDateFormat(
			Constants.DATE_FORMAT);
		private NumberFormat _nf = new DecimalFormat(
			Constants.NUMBER_FORMAT);
		private Record _record = new Record();

		public Builder() {}

		public Builder(Properties props) throws FatalException
		{
			try {
				parseProps(props);
			}
			catch (Exception e) {
				throw new FatalException(e);
			}
		}

		public Builder parseProps(Properties props)
				throws FatalException
		{
			for (String key : figureProps.keySet()) {
				String prop = PROPHEAD + key;
				Figure figure = figureProps.get(key);

				if (!props.containsKey(prop)) {
					logger.warn("\""+prop+"\" not found");
					continue;
				}
				_record.setFigure(figure,getDouble(props,prop));
			}
			for (String key : eventProps.keySet()) {
				String prop = PROPHEAD + key;
				Event event = eventProps.get(key);

				if (!props.containsKey(prop)) {
					logger.warn("\""+prop+"\" not found");
					continue;
				}
				_record.setDate(event, getDate(props, prop));
			}
			if (_record.getDate(Event.death) == null
					&& _record.getDate(Event.birth)!=null) {
				Date birth = _record.getDate(Event.birth);
				_record.setDate(Event.death,
					new Date(birth.getTime() + (DEATH_AGE
						* SECS_PER_MO * 12 * 1000)));
			}
			try {
				parseKids(props);
				parseSnowballs(props);
			}
			catch (Exception e) {
				throw new FatalException(e);
			}
			return(this);
		}

		public Builder event(Event _event, Date _date)
		{
			_record.setDate(_event, _date);
			return(this);
		}

		public Builder figure(Figure _figure, Double _amount)
		{
			_record.setFigure(_figure, _amount);
			return(this);
		}

		public Builder snowball(Double amount)
		{
			_record.setSnowball(amount);
			return(this);
		}

		public Builder kid(Date bday)
		{
			_record.setKid(bday);
			return(this);
		}

		public Builder ordinal(int _ordinal)
		{
			_record.setOrdinal(_ordinal);
			return(this);
		}

		public double getDouble(Properties props, String prop)
		{
			return(Double.parseDouble(props.getProperty(prop,"0")));
		}

		public Date getDate(Properties props, String prop)
				throws FatalException
		{
			try {
				return(_df.parse(props.getProperty(prop)));
			}
			catch (Exception e) {
				throw new FatalException("failed to get "
					+"property \""+prop+"\": "+e, e);
			}
		}

		public void parseSnowballs(Properties props)
				throws ParseException
		{
			for (Object key : props.keySet()) {
				String[] dat = (""+key).split("\\.");

				if (dat.length != 3
						|| !dat[1].equals("expense")) {
					continue;
				}
				_record.setSnowball(Double.parseDouble(
					props.getProperty(""+key)));
			}
		}

		public void parseKids(Properties props) throws ParseException
		{
			for (Object key : props.keySet()) {
				String[] dat = (""+key).split("\\.");

				if (dat.length != 3 || !dat[1].equals("kid")) {
					continue;
				}
				_record.setKid(_df.parse(
					props.getProperty(""+key)));
			}
		}

		public Record build() throws FatalException
		{
			// _record.validate();
			return(_record);
		}
	}

	public Record()
	{
		dates = new HashMap<>();
		figures = new HashMap<>();
		snowballs = new ArrayList<>();
		kids = new ArrayList<>();
	}

	public void setOrdinal(int ordinal)
	{
		this.ordinal = ordinal;
	}

	public int getOrdinal()
	{
		return(ordinal);
	}

	public void incrementOrdinal()
	{
		ordinal++;
	}

	public Date getToday()
	{
		return(getDate(Event.currentDate));
	}

	public Date getDate(Event event)
	{
		return(dates.get(event));
	}

	public void setDate(Event event, Date date)
	{
		dates.put(event, date);
	}

	public Double getBalance()
	{
		return(getFigure(Figure.capital));
	}

	public void setBalance(double amount)
	{
		setFigure(Figure.capital, amount);
	}

	public Double getEmergencyFund()
	{
		return(getFigure(Figure.emergencyFund));
	}

	public Double getMonthlySavings()
	{
		return(getFigure(Figure.monthlySavings));
	}

	public void setSnowball(Double snowball)
	{
		snowballs.add(snowball);
	}

	public void setKid(Date kid)
	{
		kids.add(kid);
	}

	public Double getFigure(Figure figure)
	{
		Double retval = 0.0;

		if (figures.containsKey(figure)) {
			retval = figures.get(figure);
		}
		return(retval);
	}

	public void setFigure(Figure figure, Double amount)
	{
		figures.put(figure, amount);
	}

	public List<Double> getSnowballs()
	{
		Collections.sort(snowballs);
		return(snowballs);
	}

	public List<Date> getKids()
	{
		Collections.sort(kids);
		return(kids);
	}

	public void enforceNonNull(Event event)
			throws InvalidRecordFatalException
	{
		if (getDate(event) == null) {
			throw new InvalidRecordFatalException(event);
		}
	}

	public void enforceNonZero(Figure figure)
			throws InvalidRecordFatalException
	{
		if (getFigure(figure) <= 0.00) {
			throw new InvalidRecordFatalException(figure);
		}
	}

	public void validate() throws InvalidRecordFatalException
	{
		logger.debug("VALIDATING: "+figures);

		if (getFigure(Figure.mortgageDebt) > 0.00) {
			enforceNonZero(Figure.mortgageInt);
			enforceNonZero(Figure.mortgageLoan);
			enforceNonZero(Figure.mortgagePayment);
			enforceNonZero(Figure.mortgageLength);
			enforceNonZero(Figure.houseValue);
		}
		for (Figure figure : Figure.values()) {
			if (getFigure(figure) >= 0.0001) {
				continue;
			}
			switch (figure) {
			case savingsInt:
			case marketRate:
			case salary:
			case monthlySavings:
			case numEmergencyMonths:
				enforceNonZero(figure);
				break;
			default:
				//logger.warn("figure \""+figure+"\" not set");
				break;
			}
		}
		for (Event event : Event.values()) {
			enforceNonNull(event);
		}
		if (snowballs.size() > 0) {
			double total = 0;

			for (Double amount : snowballs) {
				total += amount;
			}
			if (total > getFigure(Figure.nonMortgageDebt)) {
				throw new InvalidRecordFatalException(
					Reason.snowball);
			}
		}
	}

	@Override
	public Object clone() throws CloneNotSupportedException
	{
		Object retval = null;
		Builder builder = new Builder().ordinal(getOrdinal());

		for (Figure figure : Figure.values()) {
			builder.figure(figure, getFigure(figure));
		}
		for (Event event : Event.values()) {
			builder.event(event, getDate(event));
		}
		for (Date kid : getKids()) {
			builder.kid(kid);
		}
		for (Double snowball : getSnowballs()) {
			builder.snowball(snowball);
		}
		try {
			retval = builder.build();
			((Record)retval).validate();
		}
		catch (FatalException e) {
			logger.error(""+e, e);
			throw new CloneNotSupportedException(""+e);
		}
		return(retval);
	}

	@Override
	public String toString()
	{
		return(new StringBuilder()
			.append("\n=======================================")
			.append("\nRECORD")
			.append("\n\ttoday: "
				+df.format(getDate(Event.currentDate)))
			.append("\nASSETS")
			.append("\n\tretirement: $"
				+nf.format(getFigure(Figure.currRetireSavings)))
			.append("\n\tcollege: $"
				+nf.format(getFigure(Figure.collegeBalance)))
			.append("\n\temergency fund: $"
				+nf.format(getFigure(Figure.emergencyFund)))
			.append("\n\tsavings account balance (capital): $"
				+nf.format(getFigure(Figure.capital)))
			.append("\nLIABILITIES")
			.append("\n\tmortgage: $"
				+nf.format(getFigure(Figure.mortgageDebt)))
			.append("\n\tnon-mortgage debt: $"
				+nf.format(getFigure(Figure.nonMortgageDebt)))
			.append("\n=======================================")
			.toString());
	}
}
