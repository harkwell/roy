/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.util.Date;

public class MortalityException extends LifeException
{
	private static final long serialVersionUID = 0x0001L;

	public MortalityException(Date transpiredDate)
	{
		super(transpiredDate);
	}

	public MortalityException(Date transpiredDate, String message)
	{
		super(transpiredDate, message);
	}
}
