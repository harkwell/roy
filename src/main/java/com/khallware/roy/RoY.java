/**
 * ============================================================================
 * RoY.java is the main class for the Android RamseyOnYou application
 *
 * The Android "RoY" application
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import com.khallware.roy.Record.Builder;
import android.text.method.ScrollingMovementMethod;
import android.widget.TabHost.TabContentFactory;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.ScrollView;
import android.widget.LinearLayout;
import android.content.Context;
import android.app.TabActivity;
import android.view.ViewGroup;
import android.view.View;
import android.os.Bundle;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class RoY extends TabActivity implements TabContentFactory
{
	public static final String BNDLKEY_SCENARIO = "scenario";
	public static final String BNDLKEY_CHOICES = "choices";
	public static final String BNDLKEY_RECORD = "record";
	public static final String BNDLKEY_SHOW   = "showResults";

	private Logger logger = LoggerFactory.getLogger(RamseyOnYou.class);
	private DateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
	private TabHost tabHost = null;
	private enum RoYTab { home, input, step1, step2, step3, step4, step5,
		step6, step7, analysis };
	private RamseyOnYou roy = new RamseyOnYou();
	private static final Map<RoYTab, Integer> map;
	private static boolean showResults = false;
	private static boolean firstTime = false;
	private static String[] choices = null;
	private static Record record = null;
	private InputLinearLayout inputLinearLayout = null;
	private int scenario = InputLinearLayout.DEF_SCENARIO;

	static {
		map = new HashMap<RoYTab, Integer>() {{
			this.put(RoYTab.home, R.string.tab_home);
			this.put(RoYTab.input, R.string.tab_input);
			this.put(RoYTab.step1, R.string.tab_step1);
			this.put(RoYTab.step2, R.string.tab_step2);
			this.put(RoYTab.step3, R.string.tab_step3);
			this.put(RoYTab.step4, R.string.tab_step4);
			this.put(RoYTab.step5, R.string.tab_step5);
			this.put(RoYTab.step6, R.string.tab_step6);
			this.put(RoYTab.step7, R.string.tab_step7);
			this.put(RoYTab.analysis, R.string.tab_analysis);
		}};
	}

	@Override
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		int mode = Context.MODE_PRIVATE;
		Datastore.getDatastore(this, getPreferences(mode));
		logger.debug((bundle == null) ? "" : UIUtil.dumpBundle(bundle));
		setContentView(R.layout.main);
		tabHost = (TabHost)findViewById(android.R.id.tabhost);

		if ((firstTime = (bundle == null))) {
			try {
				choices = DatastoreUtil.getScenarioNames();
				record = Datastore.getDatastore().getSeedRecord(
					scenario);
			}
			catch (RoYException e) {
				logger.error(""+e, e);
			}
		}
	}

	@Override
	public void onStart()
	{
		super.onStart();
		updateTabs(showResults);
	}

	@Override
	public void onSaveInstanceState(Bundle bundle)
	{
		bundle.putInt(BNDLKEY_SCENARIO, scenario);
		bundle.putSerializable(BNDLKEY_RECORD, record);
		bundle.putSerializable(BNDLKEY_CHOICES, choices);
		bundle.putBoolean(BNDLKEY_SHOW, showResults);
		super.onSaveInstanceState(bundle);
	}

	@Override
	public void onRestoreInstanceState(Bundle bundle)
	{
		super.onRestoreInstanceState(bundle);
		try {
			scenario = bundle.getInt(BNDLKEY_SCENARIO,
				InputLinearLayout.DEF_SCENARIO);
			record = (Record)bundle.getSerializable(BNDLKEY_RECORD);
			showResults = bundle.getBoolean(BNDLKEY_SHOW);
			choices = (String[])bundle.getSerializable(
				BNDLKEY_CHOICES);
		}
		catch (Exception e) {
			logger.error(""+e, e);
		}
	}

	public void updateTabs()
	{
		updateTabs(true);
	}

	public void updateTabs(boolean includeResultTabs)
	{
		TabContentFactory factory = this;
		List<RoYTab> tabs = new ArrayList<>();
		scenario = getInputLinearLayout().getScenario();
		showResults |= includeResultTabs;
		inputLinearLayout = null;
		tabHost.clearAllTabs();
		roy.getTimeline().clear();
		tabs.add(RoYTab.home);
		tabs.add(RoYTab.input);

		if (includeResultTabs) {
			tabs.clear();
			tabs.addAll(Arrays.asList(RoYTab.values()));
		}
		for (RoYTab roytab : tabs) {
			tabHost.addTab(tabHost.newTabSpec(""+roytab)
				.setIndicator(getString(map.get(roytab)))
				.setContent(factory));
		}
	}

	/** {@inheritDoc} */
	public View createTabContent(String tag)
	{
		View retval = null;
		RoYTab roytab = null;
		Context context = getInputLinearLayout().getContext();
		Step step = null;
		int idx = 0;
		logger.debug("factory producing view, tag \""+tag+"\"");
		try {
			roytab = RoYTab.valueOf(tag);
		}
		catch (IllegalArgumentException e) {
			logger.error("unknown \""+tag+"\": "+e, e);
		}
		switch (roytab) {
		case home:
			retval = ViewFactory.make(R.layout.home, context);
			((TextView)((ViewGroup)retval).getChildAt(0)
				).setMovementMethod(
					new ScrollingMovementMethod());
			break;
		case input:
			retval = new ScrollView(context);
			retval.setId(ViewFactory.sequence++);
			((ScrollView)retval).addView(getInputLinearLayout());
			break;
		case step7:
			idx++;
		case step6:
			idx++;
		case step5:
			idx++;
		case step4:
			idx++;
		case step3:
			idx++;
		case step2:
			idx++;
		case step1:
			step = (roy.getSteps().size() > idx)
				? roy.getSteps().get(idx)
				: null;
			retval = ViewFactory.make(step, this);
			break;
		case analysis:
			try {
				retval = ViewFactory.makeChart(this);
			}
			catch (Exception e) {
				retval = ViewFactory.make(e, this);
			}
			break;
		default:
			retval = ViewFactory.make(new RoYException(
				"unhandled tab: "+roytab), this);
			logger.error("unhandled tab: "+roytab);
			break;
		};
		return(retval);
	}

	protected InputLinearLayout getInputLinearLayout()
	{
		if (inputLinearLayout != null) {
			return(inputLinearLayout);
		}
		try {
			logger.debug("creating the InputLinearLayout");
			inputLinearLayout = new InputLinearLayout(roy, this);
			inputLinearLayout.setId(ViewFactory.sequence++);
			inputLinearLayout.setup(scenario, record, this,choices);
		}
		catch (Exception e) {
			logger.error(""+e, e);
		}
		return(inputLinearLayout);
	}
}
