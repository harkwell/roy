/**
 * ============================================================================
 * Datastore.java is a helper class for dealing with stored data
 *
 * The Android "RoY" application
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import com.khallware.roy.Record.Builder;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteException;
import android.database.DatabaseErrorHandler;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Date;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.DecimalFormat;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class Datastore extends SQLiteOpenHelper
{
	public static final String DB_FILE = "roy.db";
	public static final String DB_PATH = "/data/data/com.khallware"
		+".roy/databases/";
	public static final int DB_VERSION = 1;
	public static final String DELIMETER = ",";
	private static Datastore instance = null;

	private Logger logger = LoggerFactory.getLogger(Datastore.class);
	private NumberFormat nf = new DecimalFormat(Constants.NUMBER_FORMAT);
	private DateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
	private SQLiteDatabase handle = null;
	private enum Encoded { snowball, kid };

	private Datastore(Context ctxt, SharedPreferences prefs)
	{
		super(ctxt, DB_FILE, null, DB_VERSION,
				new DatabaseErrorHandler() {
			public void onCorruption(SQLiteDatabase dbObj) {
			}
		});
	}

	public static Datastore getDatastore()
	{
		return(instance);
	}

	public static Datastore getDatastore(Context ctxt,
			SharedPreferences prefs)
	{
		if (instance != null) {
			return(instance);
		}
		instance = new Datastore(ctxt, prefs);
		return(instance);
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		logger.debug("onCreate(SQLiteDatabase)ing...");
		String sql = "CREATE TABLE records ("
			+           "scenario INTEGER NOT NULL, "
			+           "ordinal INTEGER NOT NULL, "
			+           "date DATE NOT NULL, "
			+           "key VARCHAR(255) NOT NULL, "
			+           "value VARCHAR(255) NOT NULL "
			+    ")";
		logger.debug("sql: " +sql);
		db.execSQL(sql);
		sql = "CREATE TABLE graph_data ("
			+           "scenario INTEGER NOT NULL, "
			+           "date DATE NOT NULL, "
			+           "networth DOUBLE NOT NULL"
			+    ")";
		logger.debug("sql: " +sql);
		db.execSQL(sql);
		sql = "CREATE TABLE scenarios ("
			+           "id INTEGER NOT NULL, "
			+           "name VARCHAR(255) NOT NULL"
			+    ")";
		logger.debug("sql: " +sql);
		db.execSQL(sql);

		for (int idx : new int[] { 1, 2, 3, 4, 5 }) {
			sql = "INSERT INTO scenarios (id,name) "
				+  "VALUES ("+idx+",'Scenario "+idx+"')";
			logger.debug("sql: " +sql);
			db.execSQL(sql);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer)
	{
		logger.debug("onUpgrade()ing... from "+oldVer+" to "+newVer);
		onCreate(db);
	}

	public Map<Integer, String> getScenarios() throws RoYException
	{
		logger.debug("getScenarios()ing...");
		try {
			return(getScenariosUnwrapped());
		}
		catch (Exception e) {
			if (e instanceof RoYException) {
				throw (RoYException)e;
			}
			else {
				throw new RoYException(e);
			}
		}
	}

	public void updateScenario(int scenario, String name)
			throws RoYException
	{
		logger.debug("updateScenario()ing...");
		try {
			updateScenarioUnwrapped(scenario, name);
		}
		catch (Exception e) {
			if (e instanceof RoYException) {
				throw (RoYException)e;
			}
			else {
				throw new RoYException(e);
			}
		}
	}

	public List<Step> getSteps(int scenario) throws RoYException
	{
		logger.debug("getSteps()ing...");
		try {
			return(getStepsUnwrapped(scenario));
		}
		catch (Exception e) {
			if (e instanceof RoYException) {
				throw (RoYException)e;
			}
			else {
				throw new RoYException(e);
			}
		}
	}

	public Map<Date, Double> getGraphData(int scenario) throws RoYException
	{
		logger.debug("getGraphData("+scenario+")ing...");
		try {
			return(getGraphDataUnwrapped(scenario));
		}
		catch (Exception e) {
			if (e instanceof RoYException) {
				throw (RoYException)e;
			}
			else {
				throw new RoYException(e);
			}
		}
	}

	public Record getRecord(int scenario, int ordinal) throws RoYException
	{
		logger.debug("getRecord("+scenario+", "+ordinal+")ing...");
		try {
			return(getRecordUnwrapped(scenario, ordinal));
		}
		catch (Exception e) {
			if (e instanceof RoYException) {
				throw (RoYException)e;
			}
			else {
				throw new RoYException(e);
			}
		}
	}

	public static Record getUSAverageRecord() throws RoYException,
			ParseException, FatalException
	{
		DateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
		return(new Builder()
			.figure(Figure.salary, 86700.00)
			.figure(Figure.monthlySavings, 1000.00)
			.figure(Figure.emergencyFund, 0.00)
			.figure(Figure.capital, 500.00)
			.figure(Figure.currRetireSavings, 0.00)
			.figure(Figure.collegeBalance, 0.00)
			.figure(Figure.mortgageDebt, 273769.64)
			.figure(Figure.nonMortgageDebt, 31503.14)
			.figure(Figure.moRetireSavings, 401.23)
			.figure(Figure.currMoCollegeSavings, 0.00)
			.figure(Figure.numEmergencyMonths, 6.0)
			.figure(Figure.mortgageInt, 0.0425)
			.figure(Figure.houseValue, 350000.00)
			.figure(Figure.mortgageLoan, 280000.00)
			.figure(Figure.mortgageLength, 30.0)
			.figure(Figure.mortgagePayment, 1438.99)
			.figure(Figure.savingsInt, 0.0005)
			.figure(Figure.marketRate, 0.15)
			.figure(Figure.nestEgg, 0.00)
			.event(Event.birth, df.parse("19700101"))
			.event(Event.death, df.parse("20700101"))
			.event(Event.retirement, df.parse("20400101"))
			.event(Event.currentDate, df.parse("20130101"))
			.build());
	}

	public void saveGraphData(int scenario, List<Record> timeline)
			throws RoYException
	{
		logger.debug("save()ing..."+timeline.size()+" graphdata items");
		try {
			replaceGraphData(scenario, timeline);
		}
		catch (Exception e) {
			if (e instanceof RoYException) {
				throw (RoYException)e;
			}
			else {
				throw new RoYException(e);
			}
		}
	}

	public void save(int scenario, Record seed, List<Step> steps)
			throws RoYException
	{
		logger.debug("save()ing..."+steps.size()+" steps");
		try {
			replaceRecord(scenario, seed);

			for (Step step : steps) {
				saveUnwrapped(scenario, step);
			}
		}
		catch (Exception e) {
			if (e instanceof RoYException) {
				throw (RoYException)e;
			}
			else {
				throw new RoYException(e);
			}
		}
	}

	public Record getSeedRecord(int scenario) throws RoYException
	{
		logger.debug("getSeedRecord("+scenario+")ing...");
		try {
			Record retval = getSeedRecordUnwrapped(scenario);
			return((retval == null)
				? getUSAverageRecord()
				: retval);
		}
		catch (Exception e) {
			if (e instanceof RoYException) {
				throw (RoYException)e;
			}
			else {
				throw new RoYException(e);
			}
		}
	}

	private SQLiteDatabase handle()
	{
		if (handle == null || !handle.isOpen()) {
			handle = getWritableDatabase();
		}
		return(handle);
	}

	private void addKeyValue(Builder builder, String key, String value)
			throws RoYException, IllegalArgumentException,
			ParseException
	{
		Encoded encoded = null;
		Figure figure = null;
		Event event = null;
		try {
			switch ((encoded = Encoded.valueOf(key))) {
			case kid:
				for (String k : value.split(DELIMETER)){
					builder.kid(df.parse(k));
				}
				logger.debug("added kid to record");
				break;
			case snowball:
				for (String s : value.split(DELIMETER)){
					builder.snowball(Double.parseDouble(s));
				}
				logger.debug("added snowball to record");
				break;
			}
		}
		catch (IllegalArgumentException e1) {
			try {
				event = Event.valueOf(key);
				builder.event(event, df.parse(value));
				//logger.debug("record got event: "+event);
			}
			catch (IllegalArgumentException e2) {
				figure = Figure.valueOf(key);
				builder.figure(figure,
					Double.parseDouble(value));
				//logger.debug("record got figure: "+figure);
			}
		}
	}

	private void updateScenarioUnwrapped(int scenario, String name)
			throws SQLiteException
	{
		String sql = "UPDATE scenarios "
			+       "SET name = '"+name+"' "
			+     "WHERE id = "+scenario;
		logger.debug("sql: " +sql);
		handle().execSQL(sql);
	}

	private Record getRecordUnwrapped(int scenario, int ordinal)
			throws SQLiteException, RoYException, FatalException,
			ParseException
	{
		Record retval = null;
		Builder builder = new Builder();
		Cursor cursor = null;
		boolean found = false;
		String sql = "SELECT ordinal, date, key, value "
			+      "FROM records "
			+     "WHERE scenario = "+scenario+" "
			+       "AND ordinal = "+ordinal;
		logger.debug("sql: " +sql);
		cursor = handle().rawQuery(sql, new String[] {});

		while (cursor.moveToNext()) {
			String key = cursor.getString(
				cursor.getColumnIndexOrThrow("key"));
			String value = cursor.getString(
				cursor.getColumnIndexOrThrow("value"));
			addKeyValue(builder, key, value);
			found = true;
		}
		cursor.close();
		builder.ordinal(ordinal);

		if (found) {
			retval = builder.build();
		}
		return(retval);
	}

	private Map<Integer, String> getScenariosUnwrapped()
			throws RoYException, SQLiteException, ParseException
	{
		Map<Integer, String> retval = new HashMap<>();
		Cursor cursor = null;
		String sql = "SELECT id, name "
			+      "FROM scenarios "
			+     "ORDER BY id";
		logger.debug("sql: " +sql);
		cursor = handle().rawQuery(sql, new String[] {});

		while (cursor.moveToNext()) {
			int id = cursor.getInt(
				cursor.getColumnIndexOrThrow("id"));
			String name = cursor.getString(
				cursor.getColumnIndexOrThrow("name"));
			retval.put(id, name);
		}
		cursor.close();
		logger.debug("found "+retval.size()+" records");
		return(retval);
	}

	private Map<Date, Double> getGraphDataUnwrapped(int scenario)
			throws RoYException, SQLiteException, ParseException
	{
		Map<Date, Double> retval = new HashMap<>();
		Cursor cursor = null;
		String sql = "SELECT DISTINCT date, networth "
			+      "FROM graph_data "
			+     "WHERE scenario = '"+scenario+"' "
			+     "ORDER BY date";
		logger.debug("sql: " +sql);
		cursor = handle().rawQuery(sql, new String[] {});

		while (cursor.moveToNext()) {
			Date date = df.parse(
				cursor.getString(
					cursor.getColumnIndexOrThrow("date")));
			retval.put(date,
				cursor.getDouble(
					cursor.getColumnIndexOrThrow("networth")
				));
		}
		cursor.close();
		logger.debug("found "+retval.size()+" records");
		return(retval);
	}

	private Record getSeedRecordUnwrapped(int scenario)
			throws SQLiteException, RoYException
	{
		Record retval = null;
		List<Integer> list = getRecordOrdinals(scenario);
		int ordinal = (list.size() > 0) ? list.get(0) : -1;
		retval = getRecord(scenario, ordinal);
		return(retval);
	}

	private List<Integer> getRecordOrdinals(int scenario)
			throws SQLiteException
	{
		List<Integer> retval = new ArrayList<>();
		String sql = "SELECT DISTINCT ordinal "
			+      "FROM records "
			+     "WHERE scenario = "+scenario+" "
			+     "ORDER BY ordinal";
		Cursor cursor = null;
		logger.debug("sql: " +sql);
		cursor = handle().rawQuery(sql, new String[] {});

		while (cursor.moveToNext()) {
			int ordinal = cursor.getInt(
				cursor.getColumnIndexOrThrow("ordinal"));
			retval.add(ordinal);
		}
		cursor.close();
		return(retval);
	}

	private List<Step> getStepsUnwrapped(int scenario)
			throws RoYException, SQLiteException, ParseException
	{
		List<Step> retval = new ArrayList<>();
		boolean skippedSeed = false;
		Record record = null;
		Step step = null;
		int idx = 0;

		for (int ordinal : getRecordOrdinals(scenario)) {
			if (!skippedSeed) {
				skippedSeed = true;
				continue;
			}
			record = getRecord(scenario, ordinal);

			switch (idx) {
			case 0:
				step = new Step1();
				break;
			case 1:
				step = new Step2();
				break;
			case 2:
				step = new Step3();
				break;
			case 3:
				step = new Step4();
				break;
			case 4:
				step = new Step5();
				break;
			case 5:
				step = new Step6();
				break;
			case 6:
				step = new Step7();
				break;
			}
			step.setFinalRecord(record);
			retval.add(step);
			idx++;
		}
		logger.debug("found "+retval.size()+" steps");
		return(retval);
	}

	private void replaceGraphData(int scenario, List<Record> timeline)
			throws SQLiteException
	{
		String sql = "DELETE FROM graph_data "
			+          "WHERE scenario = "+scenario;
		logger.debug("sql: " +sql);
		handle().execSQL(sql);

		for (Record record : timeline) {
			Date date = record.getToday();
			double networth = Util.networth(record);
			sql = "INSERT INTO graph_data (scenario, date, "
				+         "networth) "
				+  "VALUES ("
				+          scenario+", "
				+          df.format(date)+", "
				+      "'"+networth+"'"
				+  ")";
			logger.debug("sql: " +sql);
			handle().execSQL(sql);
		}
	}

	private void saveUnwrapped(int scenario, Step step)
			throws SQLiteException
	{
		if (step.completed()) {
			replaceRecord(scenario, step.getRecord());
		}
		else {
			logger.warn("step did not complete ("+step+")");
		}
	}

	private void replaceRecord(int scenario, Record record)
			throws SQLiteException
	{
		Date today = record.getDate(Event.currentDate);
		String encoding = "";
		String sql = "DELETE FROM records "
			+          "WHERE scenario = "+scenario+" "
			+            "AND date = "+df.format(today);
		logger.debug("sql: " +sql);
		handle().execSQL(sql);

		for (Figure figure : Figure.values()) {
			sql = "INSERT INTO records (scenario, ordinal, date, "
				+         "key, value) "
				+  "VALUES ("
				+          scenario+", "
				+          record.getOrdinal()+", "
				+          df.format(today)+", "
				+      "'"+figure+"', "
				+      "'"+record.getFigure(figure)+"'"
				+  ")";
			logger.debug("sql: " +sql);
			handle().execSQL(sql);
		}
		for (Event event : Event.values()) {
			sql = "INSERT INTO records (scenario, ordinal, date, "
				+         "key, value) "
				+  "VALUES ("
				+          scenario+", "
				+          record.getOrdinal()+", "
				+          df.format(today)+", "
				+      "'"+event+"', "
				+      "'"+df.format(record.getDate(event))+"'"
				+  ")";
			logger.debug("sql: " +sql);
			handle().execSQL(sql);
		}
		for (Double snowball : record.getSnowballs()) {
			encoding += (encoding.isEmpty()) ? "" : DELIMETER;
			encoding += nf.format(snowball);
		}
		if (!encoding.isEmpty()) {
			sql = "INSERT INTO records (scenario, ordinal, date, "
				+         "key, value) "
				+  "VALUES ("
				+          scenario+", "
				+          record.getOrdinal()+", "
				+          df.format(today)+", "
				+      "'"+Encoded.snowball+"', "
				+      "'"+encoding+"'"
				+  ")";
			logger.debug("sql: " +sql);
			handle().execSQL(sql);
			encoding = "";
		}
		for (Date kid : record.getKids()) {
			encoding += (encoding.isEmpty()) ? "" : DELIMETER;
			encoding += df.format(kid);
		}
		if (!encoding.isEmpty()) {
			sql = "INSERT INTO records (scenario, ordinal, date, "
				+         "key, value) "
				+  "VALUES ("
				+          scenario+", "
				+          record.getOrdinal()+", "
				+          df.format(today)+", "
				+          Encoded.kid+", "
				+      "'"+encoding+"'"
				+  ")";
			logger.debug("sql: " +sql);
			handle().execSQL(sql);
			encoding = "";
		}
	}
}
