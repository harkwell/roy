/**
 * ============================================================================
 * InputLinearLayout.java is the user input view for RoY
 *
 * The Android "RoY" application
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnLongClickListener;
import android.app.AlertDialog.Builder;
import android.app.AlertDialog;
import android.app.Activity;
import android.app.ProgressDialog;
import android.widget.LinearLayout;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Space;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface;
import java.util.Date;
import java.util.List;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class InputLinearLayout extends LinearLayout
{
	public static final int DEF_SCENARIO = 1;
	private static Logger logger = LoggerFactory.getLogger(
				InputLinearLayout.class);

	private static RamseyOnYou roy = null;
	private static Record record = null;
	private static int scenario = DEF_SCENARIO;
	private static Activity activity = null;
	private static ViewGroup snowballs = null;
	private static ViewGroup events = null;
	private static ViewGroup kids = null;

	private Button button = null;
	private Context context = null;
	private EditText scenarioEditText = null;

	public InputLinearLayout(RamseyOnYou roy, Context context)
	{
		super(context);
		this.context = context;
		this.roy = roy;
		scenario = 1;
		setOrientation(LinearLayout.VERTICAL);
	}

	public int getScenario()
	{
		return(scenario);
	}

	public void setup(int scenario, Record record, RoY activity,
			final String[] choices) throws RoYException
	{
		this.activity = activity;
		this.scenario = scenario;
		this.record = record;
		int idx = (scenario - 1);
		try {
			record.validate();
		}
		catch (FatalException e) {
			addView(ViewFactory.make(e, context));
			throw new RoYException(e);
		}
		addView(ViewFactory.make("SCENARIO", context));
		scenarioEditText = (EditText)ViewFactory.make(choices[idx],null,
					new OnLongClickListener() {
				public boolean onLongClick(View v) {
					getAlertDialog("Choose Scenario",
						choices, v.getContext()).show();
					return(true);
				}
			}, context);
		addView(scenarioEditText);
		//KDH clear button (removes from datastore)

		addView(ViewFactory.make("ASSETS", context));
		addView(ViewFactory.make(record, new Figure[] {
				Figure.capital, Figure.currRetireSavings,
				Figure.collegeBalance, Figure.emergencyFund
			}, context));
		addView(new Space(context));

		addView(ViewFactory.make("LIABILITIES", context));
		addView(ViewFactory.make(record, new Figure[] {
				Figure.mortgageDebt, Figure.nonMortgageDebt
			}, context));
		addView(new Space(context));

		addView(ViewFactory.make("DATES", context));
		addView(ViewFactory.make(record, Event.values(), context));
		addView(new Space(context));

		addView(ViewFactory.make("OTHER", context));
		addView(ViewFactory.make(record, new Figure[] {
				Figure.salary, Figure.marketRate,
				Figure.savingsInt, Figure.currMoCollegeSavings,
				Figure.numEmergencyMonths,
				Figure.monthlySavings, Figure.mortgagePayment,
				Figure.moRetireSavings, Figure.houseValue,
				Figure.mortgageInt, Figure.mortgageLoan,
				Figure.mortgageLength
			}, context));
		addView(new Space(context));

		addView(ViewFactory.make("SNOWBALLS", context));
		addView(new Space(context));
		addView((snowballs = (ViewGroup)ViewFactory.make(
			new Double(0.00), record.getSnowballs(), context)));

		addView(ViewFactory.make("KIDS", context));
		addView(new Space(context));
		addView((kids = (ViewGroup)ViewFactory.make(
			new Date(), record.getKids(), context)));

		addView(ViewFactory.make("LIFE EVENTS", context));
		addView(new Space(context));
		addView((events = (ViewGroup)ViewFactory.make(new Date(),
			new Double(0.00), Figure.monthlySavings,
			roy.getEventHandlers(),context)));

		addView(getCalculateButton(context, activity));
	}

	public Activity getActivity()
	{
		return(activity);
	}

	protected Button getCalculateButton(final Context context,
			final RoY roy)
	{
		if (button != null) {
			return(button);
		}
		button = new Button(context);
		button.setText(context.getString(R.string.calculate));
		button.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				try {
					execAndSave(context, roy, record,
						getScenarioName());
				}
				catch (InvalidRecordFatalException e) {
					alertOnException(e);
				}
				catch (Exception e) {
					logger.error(""+e, e);
				}
			}
		});
		return(button);
	}

	private AlertDialog getAlertDialog(String title, final String[] choices,
			Context context)
	{
		Builder retval = new Builder(context)
			.setTitle(title)
			.setCancelable(true);
		final int current = (scenario - 1);
		final RoY roy = (RoY)activity;
		retval.setSingleChoiceItems(choices, current,
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int idx) {
				Datastore datastore = Datastore.getDatastore();
				int s = (idx + 1);
				InputLinearLayout.scenario = s;
				removeAllViews();
				try {
					final String[] choices =
					DatastoreUtil.getScenarioNames();
					setup(s,datastore.getSeedRecord(s),roy,
						choices);
				}
				catch (RoYException e) {
					logger.error(""+e, e);
				}
				dialog.dismiss();
			}
		});
		return(retval.create());
	}

	private String getScenarioName()
	{
		String retval = "Scenario "+scenario;

		if (scenarioEditText != null) {
			retval = ""+scenarioEditText.getText();
		}
		return(retval);
	}

	private static void addEventHandlers(List<EventHandler> handlers,
			ViewGroup vg)
	{
		EventHandler handler = null;
		View view = null;
		int count = (vg != null) ? vg.getChildCount() : 0;

		for (int i=0; i<count; i++) {
			if ((view = vg.getChildAt(i)) == null) {
				continue;
			}
			else if (view instanceof LinearLayout) {
				handler = extractEventHandler((ViewGroup)view);

				if (handler != null) {
					handlers.add(handler);
					logger.debug("added event handler "
						+"("+handler+")");
				}
			}
		}
	}

	private static EventHandler extractEventHandler(ViewGroup vg)
	{
		EventHandler retval = null;
		Figure figure = null;
		Double amount = null;
		Date date = null;
		View view = null;
		int count = (vg != null) ? vg.getChildCount() : 0;

		for (int i=0; i<count; i++) {
			if ((view = vg.getChildAt(i)) == null) {
				continue;
			}
			else if (view instanceof EditText) {
				EditText et = (EditText)view;
				amount = Double.parseDouble(""+et.getText());
			}
			else if (view instanceof DatePicker) {
				DatePicker dp = (DatePicker)view;
				date = new Date(dp.getCalendarView().getDate());
			}
			else if (view instanceof TextView) {
				TextView tv = (TextView)view;
				figure = (Figure)UIUtil.getValuesKey(
					UIConstants.figureResourceMap,
					tv.getText());
			}
		}
		if (figure != null && amount != null && date != null) {
			retval = new FigureChangeEventHandler(
				EventHandler.Type.adjustment, figure, date,
				amount);
		}
		return(retval);
	}

	private static void updateRecord(final Record rec, ViewGroup vg)
	{
		View view = null;
		int count = (vg != null) ? vg.getChildCount() : 0;
		logger.debug("updateRecord("+vg.getClass().getName()+")ing...");

		for (int i=0; i<count; i++) {
			if ((view = vg.getChildAt(i)) == null) {
				continue;
			}
			else if (view instanceof EditText) {
				logger.debug("it's a snowball...");
				rec.setSnowball(
					Double.parseDouble(
						""+((EditText)view).getText()));
			}
			else if (view instanceof DatePicker) {
				logger.debug("it's a birthday (kid)");
				rec.setKid(new Date(((DatePicker)
					view).getCalendarView().getDate()));
			}
			else if (view instanceof ViewGroup) {
				updateRecord(rec, (ViewGroup)view);
			}
		}
	}

	private static void alertOnException(InvalidRecordFatalException e)
	{
		String message = activity.getString(R.string.invalid_input);
		Builder builder = new Builder(activity)
			.setTitle(activity.getString(R.string.input_error))
			.setCancelable(true);
		switch (e.getReason()) {
		case snowball:
			message = activity.getString(
				R.string.invalid_snowball_sum);
			break;
		case figure:
			message = activity.getString(
				R.string.invalid_figure_amount)+": \""
				+e.getFigure() +"\"";
			break;
		case event:
			message = activity.getString(
				R.string.invalid_date)+": \""+e.getEvent()+"\"";
			break;
		case unknown:
			message += ": "+e;
			break;
		}
		builder.setMessage(message);
		builder.create().show();
	}

	private static void execAndSave(final Context context,
			final RoY activity, final Record record,
			final String scenarioName)
			throws InvalidRecordFatalException
	{
		final Datastore datastore = Datastore.getDatastore();
		final ProgressDialog dialog;
		logger.debug("execAndSave()ing...");
		roy.getEventHandlers().clear();
		try {
			addEventHandlers(roy.getEventHandlers(), events);
			updateRecord(record, snowballs);
			updateRecord(record, kids);
		}
		catch (Exception e) {
			throw new InvalidRecordFatalException(e);
		}
		record.validate();
		dialog = ProgressDialog.show(context,
			context.getString(R.string.wait),
			context.getString(R.string.calculating), true);
		logger.debug("seeding with record ("+record+")");
		dialog.setOnDismissListener(new OnDismissListener() {
			public void onDismiss(DialogInterface dialog) {
				activity.updateTabs(true);
			}
		});
		new Thread(new Runnable() {
			@Override
			public void run()
			{
				try {
					try {
						roy.exec(record);
					}
					catch (LifeException e) {
						logger.warn(""+e, e);
					}
					datastore.updateScenario(scenario,
						scenarioName);
					datastore.save(scenario,
						roy.getSeedRecord(),
						roy.getSteps());
					datastore.saveGraphData(scenario,
						roy.getTimeline());
				}
				catch (Exception e) {
					logger.error(""+e, e);
				}
				dialog.dismiss();
			}
		}).start();
	}
}
