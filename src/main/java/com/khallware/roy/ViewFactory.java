/**
 * ============================================================================
 * ViewFactory.java is the class that instantiates appropriate view for RoY
 *
 * The Android "RoY" application
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import com.khallware.roy.Util;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.content.Context;
import android.content.DialogInterface;
import android.text.method.ScrollingMovementMethod;
import android.text.TextWatcher;
import android.text.Editable;
import android.graphics.Color;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.app.AlertDialog.Builder;
import android.app.AlertDialog;
import java.text.SimpleDateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.List;
import java.io.PrintStream;
import java.io.ByteArrayOutputStream;
import org.achartengine.model.XYSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.achartengine.chart.PointStyle;
import org.achartengine.ChartFactory;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class ViewFactory
{
	public static final int INIT_SEQ_VAL = 100000;
	public static int sequence = INIT_SEQ_VAL;
	private static Logger logger = LoggerFactory.getLogger(
			ViewFactory.class);
	private static DateFormat df =
			new SimpleDateFormat(Constants.DATE_FORMAT);
	private static NumberFormat nf =
			new DecimalFormat(Constants.MONEY_FORMAT);

	public static void showAsMoney()
	{
		nf = new DecimalFormat(Constants.MONEY_FORMAT);
	}

	public static void showAsNumber()
	{
		nf = new DecimalFormat(Constants.NUMBER_FORMAT);
	}

	/**
	 * Make a view for user input based on an array of Figures.
	 */
	public static View make(Record record, Figure[] figures,
			final Context context)
	{
		final LinearLayout retval = new LinearLayout(context);
		retval.setId(sequence++);
		retval.setOrientation(LinearLayout.VERTICAL);
		fillParentWidth(retval);

		for (Figure figure : figures) {
			Integer[] labels = null;
			View label = null;

			if ((labels = UIUtil.getLabelResIds(figure)) == null) {
				logger.error("missing "+figure);
				continue;
			}
			final LinearLayout row = new LinearLayout(context);
			final String help = context.getString(labels[1]);
			label = make(context.getString(labels[0]), context);
			row.setId(sequence++);
			row.setOrientation(LinearLayout.HORIZONTAL);
			fillParentWidth(row);
			row.addView(make(record, figure, context));
			row.addView(label);
			label.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					new AlertDialog.Builder(context)
						.setTitle(
							context.getString(
								R.string.help))
						.setMessage(help)
						.setCancelable(true)
						.show();
				}
			});
			retval.addView(row);
		}
		return(retval);
	}

	/**
	 * Make a view that collects user input and updates the given record.
	 */
	public static View make(final Record record, final Figure figure,
			Context context)
	{
		final EditText retval = new EditText(context);

		if (figure.equals(Figure.savingsInt)) {
			showAsNumber();
		}
		retval.setId(sequence++);
		retval.setText(nf.format(record.getFigure(figure)));
		retval.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {}
			public void beforeTextChanged(CharSequence s, int start,
					int count, int after) {}
			public void onTextChanged(CharSequence s, int start,
					int before, int count) {
				try {
					String n = (""+s).replaceAll(",","");
					record.setFigure(figure,
						Double.parseDouble(n));
				}
				catch (Exception e) {
					logger.error(""+e, e);
					record.setFigure(figure, 0.00);
				}
			}
		});
		ViewFactory.showAsMoney();
		retval.setSingleLine();
		return(retval);
	}

	/**
	 * Make a view for user input based on an array of events (dates).
	 */
	public static View make(Record record, Event[] events,
			final Context context)
	{
		final LinearLayout retval = new LinearLayout(context);
		retval.setId(sequence++);
		retval.setOrientation(LinearLayout.VERTICAL);
		fillParentWidth(retval);

		for (Event event : events) {
			Integer[] labels = null;
			View label = null;

			if ((labels = UIUtil.getLabelResIds(event)) == null) {
				logger.error("missing "+event);
				continue;
			}
			final LinearLayout row = new LinearLayout(context);
			final String help = context.getString(labels[1]);
			label = make(context.getString(labels[0]), context);
			row.setId(sequence++);
			row.setOrientation(LinearLayout.HORIZONTAL);
			fillParentWidth(row);
			row.addView(make(record, event, context));
			row.addView(label);
			label.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					new AlertDialog.Builder(context)
						.setTitle(
							context.getString(
								R.string.help))
						.setMessage(help)
						.setCancelable(true)
						.show();
				}
			});
			retval.addView(row);
		}
		return(retval);
	}

	/**
	 * Make a view for user input of a single Event (date).
	 */
	public static View make(final Record record, final Event event,
			Context context)
	{
		final DatePicker retval = new DatePicker(context);
		final Calendar cal = Calendar.getInstance();
		retval.setId(sequence++);
		cal.setTime(record.getDate(event));
		retval.getCalendarView().setDate(
			record.getDate(event).getTime());
		retval.init(cal.get(Calendar.YEAR),
			cal.get(Calendar.MONTH),
			cal.get(Calendar.DAY_OF_MONTH),
			new OnDateChangedListener() {
				public void onDateChanged(DatePicker view,
						int year, int monthOfYear,
						int dayOfMonth) {
					cal.set(Calendar.YEAR, year);
					cal.set(Calendar.MONTH, monthOfYear);
					cal.set(Calendar.DAY_OF_MONTH,
						dayOfMonth);
					record.setDate(event, cal.getTime());
				}
			});
		return(retval);
	}

	/**
	 * Make a view with an add button that collects a bunch of doubles.
	 */
	public static View make(final Double def, List<Double> list,
			final Context context)
	{
		final LinearLayout retval = new LinearLayout(context);
		final ImageButton addButton = new ImageButton(context);
		final OnClickListener listener =  new OnClickListener() {
			public void onClick(View view) {
				retval.addView(make(nf.format(def), retval,
					context));
			}
		};
		retval.setId(sequence++);
		fillParentWidth(addButton);
		//KDH call addButton.setImageDrawable()
		addButton.setOnClickListener(listener);
		retval.setOrientation(LinearLayout.VERTICAL);
		retval.addView(addButton);

		for (Double amount : list) {
			retval.addView(make(nf.format(amount),retval,context));
		}
		return(retval);
	}

	/**
	 * Make a view with an add button that collects a bunch of kids bdays.
	 */
	public static View make(final Date def, List<Date> list,
			final Context context)
	{
		final LinearLayout retval = new LinearLayout(context);
		final ImageButton addButton = new ImageButton(context);
		final OnClickListener listener =  new OnClickListener() {
			public void onClick(View view) {
				retval.addView(make(def, retval, context));
			}
		};
		retval.setId(sequence++);
		fillParentWidth(addButton);
		//KDH call addButton.setImageDrawable()
		retval.setOrientation(LinearLayout.VERTICAL);
		retval.addView(addButton);
		addButton.setOnClickListener(listener);

		for (Date date : list) {
			retval.addView(make(def, retval, context));
		}
		return(retval);
	}

	/**
	 * Make life event view.
	 */
	public static View make(final Date defDate, final Double defAmount,
			final Figure defFigure, List<EventHandler> list,
			final Context context)
	{
		final LinearLayout retval = new LinearLayout(context);
		final ImageButton addButton = new ImageButton(context);
		final OnClickListener listener =  new OnClickListener() {
			public void onClick(View view) {
				retval.addView(make(defDate, defAmount,
					defFigure,
					UIConstants.lifeEventFigures(), retval,
					context));
			}
		};
		retval.setId(sequence++);
		fillParentWidth(addButton);
		//KDH call addButton.setImageDrawable()
		retval.setOrientation(LinearLayout.VERTICAL);
		retval.addView(addButton);
		addButton.setOnClickListener(listener);

		for (EventHandler eh : list) {
			FigureChangeEventHandler handler = null;
			try {
				handler = (FigureChangeEventHandler)eh;
			}
			catch (ClassCastException e) {
				logger.error(""+e, e);
				continue;
			}
			retval.addView(make(handler.getDate(),
				handler.getAmount(), handler.getFigure(),
				Figure.values(), retval, context));
		}
		return(retval);
	}

	/**
	 * Make an editable text panel with remove button.
	 */
	public static View make(String content, final ViewGroup parent,
			Context context)
	{
		final LinearLayout retval = new LinearLayout(context);
		final EditText editText = new EditText(context);
		final Button button = new Button(context);
		retval.setId(sequence++);
		editText.setText(content);
		editText.setSingleLine();
		button.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				parent.removeView(retval);
			}
		});
		button.setText(context.getString(R.string.remove));
		retval.addView(editText);
		retval.addView(button);
		return(retval);
	}

	/**
	 * Make a panel having a date picker with remove button.
	 */
	public static View make(Date date, final ViewGroup parent,
			Context context)
	{
		final LinearLayout retval = new LinearLayout(context);
		final DatePicker datePicker = new DatePicker(context);
		final EditText editText = new EditText(context);
		final Button button = new Button(context);
		retval.setId(sequence++);
		editText.setId(sequence++);
		datePicker.getCalendarView().setDate(date.getTime());
		button.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				parent.removeView(retval);
			}
		});
		button.setText(context.getString(R.string.remove));
		retval.addView(datePicker);
		retval.addView(button);
		return(retval);
	}

	/**
	 * Make a panel having a date picker, edit text and enum array with
	 * remove button.
	 */
	public static View make(Date date, Double amount, final Figure def,
			final Figure[] figures, final ViewGroup parent,
			final Context context)
	{
		final LinearLayout retval = (LinearLayout)
			make(R.layout.life_events, context);
		final DatePicker datePicker = (DatePicker)
			retval.findViewById(R.id.when);
		final EditText editText = (EditText)
			retval.findViewById(R.id.amount);
		final TextView textView = (TextView)
			retval.findViewById(R.id.figure);
		final Button button = (Button)
			retval.findViewById(R.id.remove);
		final Builder builder = new Builder(context)
			.setTitle(context.getString(R.string.choose_figure))
			.setCancelable(true);
		final String[] choices = new String[figures.length];
		final DialogInterface.OnClickListener listener =
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int idx) {
				Figure figure = figures[idx];
				logger.debug("chose item "+idx+": "+figure);
				try {
					textView.setText(context.getString(
						UIConstants.figureResourceMap
							.get(figure)));
				}
				catch (Exception e) {
					logger.error(""+e, e);
				}
				dialog.dismiss();
			}
		};
		int idx = 0;

		for (Figure figure : figures) {
			if (UIConstants.figureResourceMap.containsKey(figure)){
				int resId = UIConstants.figureResourceMap.get(
					figure);
				choices[idx] = context.getString(resId);
			}
			else {
				choices[idx] = ""+figure;
				logger.error("mismatching figure: "+figure);
			}
			idx++;
		}
		textView.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				builder.setSingleChoiceItems(choices, -1,
					listener);
				builder.create().show();
			}
		});
		textView.setText(
			context.getString(
				UIConstants.figureResourceMap.get(def)));
		datePicker.getCalendarView().setDate(date.getTime());
		showAsNumber();
		editText.setText(nf.format(amount));
		showAsMoney();
		button.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				parent.removeView(retval);
			}
		});
		return(retval);
	}

	/**
	 * Make a long clickable view for user input given default text.
	 */
	public static View make(String content, TextWatcher watcher,
			OnLongClickListener listener, Context context)
	{
		final EditText retval = new EditText(context);
		retval.setId(sequence++);

		if (listener != null) {
			retval.setOnLongClickListener(listener);
		}
		if (watcher != null) {
			retval.addTextChangedListener(watcher);
		}
		retval.setText(content);
		retval.setSingleLine();
		return(retval);
	}

	/**
	 * Make a view for analysis reporting (achart).
	 */
	public static View makeChart(Context context) throws RoYException
	{
		View retval = null;
		Datastore datastore = Datastore.getDatastore();
		Map<Integer, String> map = datastore.getScenarios();
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		XYSeriesRenderer seriesRenderer = new XYSeriesRenderer();
		XYMultipleSeriesRenderer renderer = null;
		PointStyle[] styles = new PointStyle[] {
			PointStyle.CIRCLE, PointStyle.CIRCLE, PointStyle.CIRCLE,
			PointStyle.CIRCLE, PointStyle.CIRCLE
			//PointStyle.X, PointStyle.DIAMOND, PointStyle.TRIANGLE,
			//PointStyle.SQUARE, PointStyle.CIRCLE
		};
		int[] colors = new int[] {
			Color.BLUE, Color.CYAN, Color.MAGENTA, Color.LTGRAY,
			Color.GREEN
		};
		int count = 0;
		renderer = new XYMultipleSeriesRenderer();

		for (int scenario : map.keySet()) {
			int idx = (count % colors.length);
			int scale = 0;
			XYSeries series = new XYSeries(map.get(scenario),scale);
			Map<Date, Double> graphData =
				datastore.getGraphData(scenario);
			seriesRenderer = new XYSeriesRenderer();
			seriesRenderer.setColor(colors[idx]);
			idx = (count % styles.length);
			seriesRenderer.setPointStyle(styles[idx]);

			for (Date today : graphData.keySet()) {
				Double x = Double.parseDouble(df.format(today));
				series.add(x, graphData.get(today));
			}
			dataset.addSeries(series);
			renderer.addSeriesRenderer(seriesRenderer);
			count++;
		}
		renderer.setChartTitleTextSize(20);
		renderer.setAxisTitleTextSize(16);
		renderer.setLabelsTextSize(15);
		renderer.setLegendTextSize(15);
		renderer.setPointSize(5f);
		renderer.setMargins(new int[] { 20, 30, 15, 20 });
		retval = ChartFactory.getLineChartView(context, dataset,
			renderer);
		return(retval);
	}

	/**
	 * A method to inflate a given view id.
	 */
	public static View make(int viewId, Context context)
	{
		View retval = null;
		LayoutInflater inflater = LayoutInflater.from(context);
		try {
			retval = inflater.inflate(viewId, null, false);
		}
		catch (InflateException e) {
			retval = make(e, context);
		}
		return(retval);
	}

	/**
	 * Make a view based on a given RamseyOnYou Step.
	 */
	public static View make(Step step, Context context)
	{
		TextView retval = new TextView(context);
		StringBuilder sb = new StringBuilder();
		Record rec = null;
		retval.setId(sequence++);
		retval.setMovementMethod(new ScrollingMovementMethod());
		try {
			if (step == null || !step.completed()) {
				int id = R.string.not_calculated;
				sb.append(context.getString(id));
			}
			else {
				logger.debug("contents: "+step.summarize());
				rec = step.getRecord();
				sb.append(""+step+"\n\n");
				sb.append(context.getString(
					R.string.step_done));
				sb.append(" "+df.format(
					rec.getDate(Event.currentDate)));
				sb.append("\n");

				sb.append(context.getString(
					R.string.step_balance));
				sb.append(" $"+nf.format(Util.networth(rec)));
				sb.append("\n");

				sb.append(context.getString(
					R.string.step_savings));
				sb.append(" $"+nf.format(
					rec.getFigure(Figure.monthlySavings)));
				sb.append("\n\n");

				if (step instanceof Step1) {
					sb.append(context.getString(
						R.string.step1));
				}
				else if (step instanceof Step2) {
					sb.append(context.getString(
						R.string.step2));
				}
				else if (step instanceof Step3) {
					sb.append(context.getString(
						R.string.step3));
				}
				else if (step instanceof Step4) {
					sb.append(context.getString(
						R.string.step4));
				}
				else if (step instanceof Step5) {
					sb.append(context.getString(
						R.string.step5));
				}
				else if (step instanceof Step6) {
					sb.append(context.getString(
						R.string.step6));
				}
				else if (step instanceof Step7) {
					sb.append(context.getString(
						R.string.step7));
				}
			}
			retval.setText(""+sb);
			retval.setClickable(false);
		}
		catch (Exception e) {
			retval = (TextView)make(e, context);
		}
		return(retval);
	}

	/**
	 * Make a simple output view having the given message.
	 */
	public static View make(String message, Context context)
	{
		final TextView retval = new TextView(context);
		retval.setId(sequence++);
		retval.setMovementMethod(new ScrollingMovementMethod());
		retval.setText(message);
		retval.setClickable(false);
		return(retval);
	}

	/**
	 * Make a simple output view displaying the given exception.
	 */
	public static View make(Exception exception, Context context)
	{
		final TextView retval = new TextView(context);
		StringBuilder message = new StringBuilder();
		ByteArrayOutputStream bos = null;
		retval.setId(sequence++);
		retval.setMovementMethod(new ScrollingMovementMethod());
		message.append(exception.getMessage());

		if (!(exception instanceof LifeException)) {
			bos = new ByteArrayOutputStream();
			exception.printStackTrace(new PrintStream(bos));
			message.append("\n");
			message.append(""+bos);
		}
		logger.debug("contents: "+message);
		retval.setText(""+message);
		retval.setClickable(false);
		return(retval);
	}

	/**
	 * Provision the View to have the width of its parent container.
	 */
	public static void fillParentWidth(View view)
	{
		LayoutParams parms = getLayoutParams(view);
		parms.width = LayoutParams.FILL_PARENT;
		view.setLayoutParams(parms);
	}

	/**
	 * Provision the View to have the height of its parent container.
	 */
	public static void fillParentHeight(View view)
	{
		LayoutParams parms = getLayoutParams(view);
		parms.height = LayoutParams.FILL_PARENT;
		view.setLayoutParams(parms);
	}

	/**
	 * Return the View's android UI LayoutParams or create them if null.
	 */
	public static LayoutParams getLayoutParams(View view)
	{
		LayoutParams retval = view.getLayoutParams();

		if (retval == null) {
			retval = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		}
		return(retval);
	}
}
