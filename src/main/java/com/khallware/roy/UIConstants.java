/**
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.util.Map;
import java.util.HashMap;

public class UIConstants
{
	public static final Map<Figure, Integer> figureResourceMap;
	public static final Map<Figure, Integer> figureHelpResourceMap;
	public static final Map<Event, Integer> eventResourceMap;
	public static final Map<Event, Integer> eventHelpResourceMap;

	static {
		figureResourceMap = new HashMap<Figure, Integer>() {{
			this.put(Figure.mortgageDebt, R.string.mortgageDebt);
			this.put(Figure.nonMortgageDebt,
				R.string.nonMortgageDebt);

			this.put(Figure.currRetireSavings,
				R.string.currRetireSavings);
			this.put(Figure.collegeBalance,
				R.string.collegeBalance);
			this.put(Figure.emergencyFund, R.string.emergencyFund);
			this.put(Figure.capital, R.string.capital);

			this.put(Figure.salary, R.string.salary);
			this.put(Figure.houseValue, R.string.houseValue);
			this.put(Figure.marketRate, R.string.marketRate);
			this.put(Figure.savingsInt, R.string.savingsInt);
			this.put(Figure.mortgageInt, R.string.mortgageInt);
			this.put(Figure.mortgageLoan, R.string.mortgageLoan);
			this.put(Figure.mortgageLength,
				R.string.mortgageLength);
			this.put(Figure.mortgagePayment,
				R.string.mortgagePayment);
			this.put(Figure.currMoCollegeSavings,
				R.string.currMoCollegeSavings);
			this.put(Figure.numEmergencyMonths,
				R.string.numEmergencyMonths);
			this.put(Figure.moRetireSavings,
				R.string.moRetireSavings);
			this.put(Figure.monthlySavings,
				R.string.monthlySavings);
			this.put(Figure.nestEgg, R.string.nestEgg);
		}};

		eventResourceMap = new HashMap<Event, Integer>() {{
			this.put(Event.birth, R.string.birth);
			this.put(Event.death, R.string.death);
			this.put(Event.retirement, R.string.retirement);
			this.put(Event.currentDate, R.string.currentDate);
		}};

		eventHelpResourceMap = new HashMap<Event, Integer>() {{
			this.put(Event.birth, R.string.hlp_birth);
			this.put(Event.death, R.string.hlp_death);
			this.put(Event.retirement, R.string.hlp_retirement);
			this.put(Event.currentDate, R.string.hlp_currentDate);
		}};

		figureHelpResourceMap = new HashMap<Figure, Integer>() {{
			this.put(Figure.capital, R.string.hlp_capital);
			this.put(Figure.currRetireSavings,
				R.string.hlp_currRetireSavings);
			this.put(Figure.collegeBalance,
				R.string.hlp_collegeBalance);
			this.put(Figure.emergencyFund,
				R.string.hlp_emergencyFund);
			this.put(Figure.mortgageDebt,
				R.string.hlp_mortgageDebt);
			this.put(Figure.nonMortgageDebt,
				R.string.hlp_nonMortgageDebt);
			this.put(Figure.salary, R.string.hlp_salary);
			this.put(Figure.marketRate, R.string.hlp_marketRate);
			this.put(Figure.savingsInt, R.string.hlp_savingsInt);
			this.put(Figure.currMoCollegeSavings,
				R.string.hlp_currMoCollegeSavings);
			this.put(Figure.numEmergencyMonths,
				R.string.hlp_numEmergencyMonths);
			this.put(Figure.monthlySavings,
				R.string.hlp_monthlySavings);
			this.put(Figure.mortgagePayment,
				R.string.hlp_mortgagePayment);
			this.put(Figure.moRetireSavings,
				R.string.hlp_moRetireSavings);
			this.put(Figure.houseValue, R.string.hlp_houseValue);
			this.put(Figure.mortgageInt, R.string.hlp_mortgageInt);
			this.put(Figure.mortgageLoan,
				R.string.hlp_mortgageLoan);
			this.put(Figure.mortgageLength,
				R.string.hlp_mortgageLength);
		}};
	}


	public static Figure[] lifeEventFigures()
	{
		return(new Figure[] {
			Figure.mortgageDebt,
			Figure.nonMortgageDebt,
			Figure.currRetireSavings,
			Figure.collegeBalance,
			Figure.emergencyFund,
			Figure.capital,
			Figure.salary,
			Figure.houseValue,
			Figure.marketRate,
			Figure.savingsInt,
			Figure.mortgageInt,
			Figure.mortgageLoan,
			Figure.mortgageLength,
			Figure.mortgagePayment,
			Figure.currMoCollegeSavings,
			Figure.numEmergencyMonths,
			Figure.moRetireSavings,
			Figure.monthlySavings,
			Figure.nestEgg
		});
	}
}
