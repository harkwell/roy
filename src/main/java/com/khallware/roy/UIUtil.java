/**
 * ============================================================================
 * UIUtil.java is a utility class for the graphical user interface
 *
 * The Android "RoY" application
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.util.Map;
import android.os.Bundle;
import android.util.SparseArray;

public class UIUtil
{
	public static Object getValuesKey(Map map, Object value)
	{
		if (map == null || !map.containsValue(value)) {
			return(null);
		}
		Object retval = null;

		for (Object key : map.keySet()) {
			if (map.get(key).equals(value)) {
				retval = map.get(key);
				break;
			}
		}
		return(retval);
	}

	public static Integer[] getLabelResIds(Figure figure)
	{
		Integer[] retval = null;
		Integer label = UIConstants.figureResourceMap.get(figure);
		Integer help = UIConstants.figureHelpResourceMap.get(figure);

		if (label != null && help != null) {
			retval = new Integer[] { label, help };
		}
		return(retval);
	}

	public static Integer[] getLabelResIds(Event event)
	{
		Integer[] retval = null;
		Integer label = UIConstants.eventResourceMap.get(event);
		Integer help = UIConstants.eventHelpResourceMap.get(event);

		if (label != null && help != null) {
			retval = new Integer[] { label, help };
		}
		return(retval);
	}

	public static String dumpBundle(Bundle bundle)
	{
		StringBuilder retval = new StringBuilder();
		retval.append(""+bundle+"\n");

		for (String key : bundle.keySet()) {
			Object obj = bundle.get(key);
			retval.append(""+key+"=("+obj+")\n");

			if (obj instanceof Bundle) {
				retval.append(dumpBundle((Bundle)obj));
			}
			else if ("android:views".equals(key)) {
				SparseArray sarray =
					bundle.getSparseParcelableArray(key);

				for (int i=0; i<sarray.size(); i++) {
					retval.append(""+sarray.keyAt(i)
						+"=("+sarray.valueAt(i)+")\n");
				}
			}
		}
		return(""+retval);
	}
}
