/**
 * ============================================================================
 * Datastore.java is a helper class for dealing with stored data
 *
 * The Android "RoY" application
 * Copyright (C) 2013-2017 Kevin D.Hall
 * All rights reserved.
 *
 * This software is open source and is released under the GPL-3.0 license.
 * https://opensource.org/licenses/GPL-3.0/
 *
 */
package com.khallware.roy;

import java.util.Collection;

public class DatastoreUtil
{
	public static String[] getScenarioNames() throws RoYException
	{
		Collection<String> c =
			Datastore.getDatastore().getScenarios().values();
		return(c.toArray(new String[c.size()]));
	}
}
